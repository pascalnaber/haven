# Haven - Devstack - Usage for DevOps engineers

WIP: See [FIXME](FIXME).

Haven Devstack has been configured to mimic a real OpenStack environment compatible with Haven as closely as possible.
This allows an engineer to play around with all components without the risk of something bad happening.

## Running

Simply run `vagrant up`. The initial provisioning will take about 30 minutes (many downloads). Optional but recommended: install the vagrant plugins first (see [Vagrantfile](Vagrantfile)).

OpenStack running in a Virtualbox VM is now directly accessible at 10.0.10.2.

- Access OpenStack Horizon dashboard at http://10.0.10.2/ (l/p demo/admin for demo project, or use admin/admin).
- SSH into your OpenStack VM using `vagrant ssh` to start using commands like `openstack server list`.
- Create a Haven Kubernetes cluster using `haven devcluster` to access the console and run `create`.

Note: A reboot of the VM requires stack.sh to be executed again, which vagrant will do yo for you automatically. You can add OFFLINE=True to local.conf inside the VM to run stack.sh without internet.
Much faster would be to `vagrant suspend` when job done and `vagrant up` when ready for more work. Also remember Virtualbox allows you to snapshot and restore your current working state using `vagrant push` and `vagrant pop`.

# Networking

Instances running inside your OpenStack VM can access eachother on the private 10.0.100.0/24 subnet.
OpenStack VM can access Instances on the public 10.0.200.0/24 subnet, for which you assign floating ip's to Instances.

Both subnets are best usable with the OpenStack 'demo' project, you should start working from there.

ICMP (ping) and SSH (port 22) have already been opened by bootstrap-phase2.sh.
Needing any other ports to be opened you should add rules to security groups, or disable firewalling completely in local.conf.

You can also access OpenStack Instances on the public subnet from your host (e.g. laptop) directly.
If you have not installed the vagrant plugins, you have to add a route on your host for public subnet 10.0.200.0/24 using gateway 10.0.10.2.
For example on macOS: `sudo route -n add -net 10.0.200 10.0.10.2`.

Creating a Haven Kubernetes cluster creates a separate internal network as usual.

# License
Copyright © VNG Realisatie 2019-2020
Licensed under the EUPL
