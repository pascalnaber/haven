#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -xe

# Cleanup.
sudo rm -rf /home/vagrant/src

# Shell.
echo '. ~/devstack/openrc admin' >> ~/.bashrc

# Enables ping and ssh to instances by adding rules to the default security group.
. /home/vagrant/devstack/openrc demo

openstack security group rule create --proto icmp --dst-port 0  default
openstack security group rule create --proto tcp  --dst-port 22 default

# Creates Kubernetes flavors.
. /home/vagrant/devstack/openrc admin

openstack flavor create --id k8sM --ram 2048 --disk 10 --vcpus 1 k8smaster
openstack flavor create --id k8sW --ram 2048 --disk 10 --vcpus 1 k8sworker

# Installs Docker.
sudo apt-get -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker vagrant

# Provisions Haven.
sudo docker pull registry.gitlab.com/commonground/haven/haven:latest

sudo curl -o /usr/local/bin/haven https://gitlab.com/commonground/haven/-/raw/master/kops/haven.sh?inline=false
sudo sed -i "s|.*gpg -e.*|echo \$MASTERKEY > ~/.haven/state/devcluster/master.key.gpg|g" /usr/local/bin/haven
sudo sed -i "s|.*gpg -d.*|MASTERKEY=\$(cat ~/.haven/state/devcluster/master.key.gpg)|g" /usr/local/bin/haven
sudo chmod 755 /usr/local/bin/haven

mkdir -p ~/.haven/state/devcluster

touch ~/.haven/state/devcluster/master.key.admins
curl -o ~/.haven/state/devcluster/cluster.env https://gitlab.com/commonground/haven/-/raw/feature/devstack-upgrade/devstack/cluster.env?inline=false
cp ~/devstack/accrc/demo/admin ~/.haven/state/devcluster/openrc.sh
echo "export OS_REGION_NAME=RegionOne" >> ~/.haven/state/devcluster/openrc.sh

ssh-keygen -t rsa -b 4096 -N '' -f ~/.haven/state/devcluster/ssh_id_rsa

chown -R vagrant:vagrant ~/.haven ~/.ssh
