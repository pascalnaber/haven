#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -xe

OPENSTACK_VERSION=train

sudo apt-get update
DEBIAN_FRONTEND=noninteractive sudo apt-get -y upgrade

git clone git://github.com/openstack-dev/devstack --branch stable/$OPENSTACK_VERSION

cp /vagrant/local.conf devstack/
sed -i "s/%OPENSTACK_VERSION%/stable\/$OPENSTACK_VERSION/" devstack/local.conf
