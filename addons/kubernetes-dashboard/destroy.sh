#!/bin/bash

# Copyright © VNG Realisatie 2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will DESTROY kubernetes-dashboard.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/kubernetes-dashboard

helm uninstall -n kubernetes-dashboard oauth2-proxy
kubectl delete -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml

rm -r $DEPLOYPATH

echo -e "\n\n[Haven] Destroyed kubernetes-dashboard.\n\n"
