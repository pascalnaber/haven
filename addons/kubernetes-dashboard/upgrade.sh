#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will upgrade kubernetes-dashboard on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DASHBOARD_DEPLOYPATH=$STATEPATH/addons/kubernetes-dashboard
OIDC_DEPLOYPATH=$STATEPATH/addons/oidc

cp $DASHBOARD_DEPLOYPATH/oauth2-proxy-values.yaml /tmp/oauth2-proxy-values.yaml && \
sed -i "s/%OIDC_CLIENT_SECRET%/$(getsecret oidc_client_secret)/g" /tmp/oauth2-proxy-values.yaml && \
sed -i "s/%OAUTH2_PROXY_COOKIE_SECRET%/$(getsecret oauth2_proxy_cookie_secret)/g" /tmp/oauth2-proxy-values.yaml && \

helm repo add commonground https://charts.commonground.nl/
helm repo update

helm upgrade oauth2-proxy \
    --namespace kubernetes-dashboard \
    -f /tmp/oauth2-proxy-values.yaml \
    commonground/oauth2-proxy && \
rm /tmp/oauth2-proxy-values.yaml

echo -e "\n[Haven] Upgraded kubernetes-dashboard.\n"
