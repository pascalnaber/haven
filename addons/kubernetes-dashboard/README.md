# Haven - Kubernetes dashboard

This addon installs the [Kubernetes dashboard](https://github.com/kubernetes/dashboard) and exposes the dashboard through an ingress that is protected by [oauth2-proxy](https://github.com/oauth2-proxy/oauth2-proxy).

## Prerequisites

Make sure you installed the [Traefik addon](../traefik/README.md).

## Install

From Haven run `addons/kubernetes-dashboard/provision.sh`. The script will guide you through what you have to do to prepare for a deployment. Make sure provided hostnames have their DNS resolve to the ingress controller.

To get the status of the deployment run `addons/kubernetes-dashboard/status.sh`

## Cleanup

From Haven run `addons/kubernetes-dashboard/destroy.sh`.

## License
Copyright © VNG Realisatie 2020
Licensed under the EUPL
