#!/bin/bash

# Copyright © VNG Realisatie 2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will provision Kubernetes Dashboard on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DASHBOARD_DEPLOYPATH=$STATEPATH/addons/kubernetes-dashboard
OIDC_DEPLOYPATH=$STATEPATH/addons/oidc

if [[ ! -f $OIDC_DEPLOYPATH/dex-values.yaml ]]; then
    echo "[Haven] Kubernetes dashboard depends on the oidc addon. Install the oidc addon first."
    exit 1
fi

if [[ ! -d $DASHBOARD_DEPLOYPATH ]]; then
    mkdir -p $DASHBOARD_DEPLOYPATH
fi

if [[ ! -f $DASHBOARD_DEPLOYPATH/kubernetes-dashboard.host ]]; then
    echo "[Haven] First create $DASHBOARD_DEPLOYPATH/kubernetes-dashboard.host (see dist/ for an example)."
    exit 1
fi

# Copy dist files to non-existing deploy files.
echo -e "\n[Haven] Copying kubernetes-dashboard distribution files to state directory without overwriting existing files."
false | cp -i /opt/haven/addons/kubernetes-dashboard/dist/* $DASHBOARD_DEPLOYPATH/

# Patch config files.
echo -e "\n\n[Haven] Patching config files with variables."
DEX_HOST=$(cat $OIDC_DEPLOYPATH/dex.host)
KUBERNETES_DASHBOARD_HOST=$(cat $DASHBOARD_DEPLOYPATH/kubernetes-dashboard.host)
sed -i "s|%DEX_HOST%|${DEX_HOST}|g" $DASHBOARD_DEPLOYPATH/oauth2-proxy-values.yaml
sed -i "s|%KUBERNETES_DASHBOARD_HOST%|${KUBERNETES_DASHBOARD_HOST}|g" $DASHBOARD_DEPLOYPATH/oauth2-proxy-values.yaml

echo "[Haven] Installing kubernetes-dashboard."
curl https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.1/aio/deploy/recommended.yaml | \
sed "/image\: kubernetesui\/dashboard/ s/\$/\n          resources\:\n            requests:\n              cpu: 50m\n              memory\: 100Mi\n            limits\:\n              cpu: 100m\n              memory: 300Mi/" | \
sed "/image\: kubernetesui\/metrics-scraper/ s/\$/\n          resources\:\n            requests:\n              cpu: 50m\n              memory\: 100Mi\n            limits\:\n              cpu: 100m\n              memory: 300Mi/" | \
kubectl apply -f -

echo "[Haven] Installing oauth2-proxy chart."
cp $DASHBOARD_DEPLOYPATH/oauth2-proxy-values.yaml /tmp/oauth2-proxy-values.yaml
sed -i "s/%OIDC_CLIENT_SECRET%/$(getsecret oidc_client_secret)/g" /tmp/oauth2-proxy-values.yaml
setsecret oauth2_proxy_cookie_secret $(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c32)
sed -i "s/%OAUTH2_PROXY_COOKIE_SECRET%/$(getsecret oauth2_proxy_cookie_secret)/g" /tmp/oauth2-proxy-values.yaml

helm repo add commonground https://charts.commonground.nl/
helm repo update
helm upgrade --install oauth2-proxy \
    --namespace kubernetes-dashboard \
     -f /tmp/oauth2-proxy-values.yaml \
    commonground/oauth2-proxy

rm /tmp/oauth2-proxy-values.yaml

# User config.
echo -e "\n[Haven] Open another terminal on your local machine and edit '~/.haven/state/$CLUSTER/addons/oidc/dex-values.yaml' and add 'https://$KUBERNETES_DASHBOARD_HOST/oauth2/callback' to the redirectURIs of the static client 'kubernetes'.\n\nPress ENTER when ready...\n\n"
read

echo "[Haven] Upgrade OIDC."
bash /opt/haven/addons/oidc/upgrade.sh

echo -e "\n[Haven] Deployed Kubernetes dashboard.\n"
