# Haven - Traefik

This addon installs the [Traefik](https://docs.traefik.io/) ingress controller. Also [cert-manager](https://cert-manager.io/docs/) is installed for automated certificate management with [Let's Encrypt](https://letsencrypt.org/).

From Haven run `addons/traefik/provision.sh`.

## Upgrade

From Haven run `addons/traefik/upgrade.sh`.

## Cleanup

From Haven run `addons/traefik/destroy.sh`.

## License
Copyright © VNG Realisatie 2019-2020
Licensed under the EUPL
