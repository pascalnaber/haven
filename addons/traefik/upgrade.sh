#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will upgrade Traefik and cert-manager on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/traefik

if [[ ! -d $DEPLOYPATH ]]; then
    echo "$DEPLOYPATH does not exist, nothing to upgrade!"

    exit 1
fi

# Copy dist files to non-existing deploy files.
echo -e "\n[Haven] Copying distribution files to state directory without overwriting existing files."

false | cp -i /opt/haven/addons/traefik/dist/* $DEPLOYPATH/

# Upgrade Traefik.
echo -e "\n\n[Haven] Upgrading Traefik."

helm repo add traefik https://containous.github.io/traefik-helm-chart
helm repo update

helm upgrade --install \
    traefik-$CLUSTER traefik/traefik \
    --namespace traefik \
    --values $DEPLOYPATH/traefik-values.yml

# Traefik needs to be restarted after applying the ssl-configuration (configmap).
kubectl apply -f $DEPLOYPATH/ssl-configuration.yml
kubectl -n traefik rollout restart deployment traefik-$CLUSTER

# Upgrade cert-manager.
# See: https://cert-manager.io/docs/installation/upgrading/
echo -e "\n\n[Haven] Upgrading cert-manager to ${CERT_MANAGER_VERSION}."

helm repo add jetstack https://charts.jetstack.io
helm repo update

kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v${CERT_MANAGER_VERSION}/cert-manager.crds.yaml

helm upgrade \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version "v${CERT_MANAGER_CHART_VERSION}"

## Done.
echo -e "\n\n[Haven] Upgraded Traefik and cert-manager.\n\n"
