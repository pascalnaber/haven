#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will DESTROY Traefik and cert-manager on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/traefik

if [[ ! -d $DEPLOYPATH ]]; then
    echo "$DEPLOYPATH does not exist, nothing to destroy!"

    exit 1
fi

helm uninstall -n traefik traefik-$CLUSTER
helm uninstall -n cert-manager cert-manager

kubectl delete -f $DEPLOYPATH/ssl-configuration.yml
kubectl delete -f $DEPLOYPATH/letsencrypt-prod.yml

rm -r $DEPLOYPATH

echo -e "\n\n[Haven] Destroyed Traefik and cert-manager.\n\nIf your traefik namespace is now empty, you are free to delete it.\n\n"
