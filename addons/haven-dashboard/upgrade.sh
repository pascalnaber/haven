#!/bin/bash

# Copyright © VNG Realisatie 2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will upgrade Haven dashboard on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/haven-dashboard

if [[ ! -d $DEPLOYPATH ]]; then
    mkdir -p $DEPLOYPATH
fi

# Get secrets.
ADMIN_PASSWORD=$(getsecret haven_dashboard_admin_password)
OIDC_CLIENT_SECRET=$(getsecret haven_dashboard_oidc_client_secret)

# Deploy Helm charts.
cp $DEPLOYPATH/dex-values.yaml /tmp/dex-values.yaml
ADMIN_PASSWORD_HASH=$(htpasswd -bnBC 10 "" ${ADMIN_PASSWORD} | cut -d ':' -f 2)
sed -i "s|%ADMIN_PASSWORD_HASH%|${ADMIN_PASSWORD_HASH}|g" /tmp/dex-values.yaml
sed -i "s|%OIDC_CLIENT_SECRET%|${OIDC_CLIENT_SECRET}|g" /tmp/dex-values.yaml

helm repo add commonground https://charts.commonground.nl/
helm repo update

helm upgrade dex commonground/dex \
    --namespace haven-dashboard \
    --values /tmp/dex-values.yaml

rm /tmp/dex-values.yaml

cp $DEPLOYPATH/dashboard-values.yaml /tmp/dashboard-values.yaml
sed -i "s|%OIDC_CLIENT_SECRET%|${OIDC_CLIENT_SECRET}|g" /tmp/dashboard-values.yaml
sed -i "s|%SECRET_KEY%|$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c32)|g" /tmp/dashboard-values.yaml

helm upgrade haven-dashboard commonground/haven-dashboard \
    --namespace haven-dashboard \
    --values /tmp/dashboard-values.yaml

rm /tmp/dashboard-values.yaml

## Done.
echo -e "\n\n[Haven] ** Haven dashboard email: admin@haven.vng.cloud password: $ADMIN_PASSWORD **"

echo -e "\n[Haven] Upgraded Haven dashboard.\n"
