#!/bin/bash

# Copyright © VNG Realisatie 2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will DESTROY haven-dashboard.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/haven-dashboard

helm uninstall -n haven-dashboard dex
helm uninstall -n haven-dashboard haven-dashboard

rm -r $DEPLOYPATH

echo -e "\n\n[Haven] Destroyed haven-dashboard.\n\nIf the haven-dashboard namespace is now empty, you are free to delete it.\n\n"
