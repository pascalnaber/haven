#!/usr/bin/env bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will provision Haven resource quota's and network policies on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

echo -ne "[Haven] enter the target namespace: "; read NAMESPACE; echo; [[ -z ${NAMESPACE} ]] && (echo "Missing namespace"; exit 1)

kubectl describe ns ${NAMESPACE} || ( echo "Namespace does not exist" ;exit 1)

helm -n ${NAMESPACE} upgrade --install haven-netpol charts/haven-netpol
helm -n ${NAMESPACE} upgrade --install haven-quota-limitrange charts/haven-quota-limitrange
