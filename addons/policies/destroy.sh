#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will remove Haven network policies and resource quota's.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

echo -ne "[Haven] enter the target namespace: "; read NAMESPACE; echo; [[ -z "${NAMESPACE}" ]] && (echo "Missing namespace"; exit 1)

kubectl describe ns "${NAMESPACE}" || ( echo "Namespace does not exist" ;exit 1)
helm -n "${NAMESPACE}" uninstall haven-netpol
helm -n "${NAMESPACE}" uninstall haven-quota-limitrange

echo -e "\n\n[Haven] Haven network policies and resource quota's removed.\n\n"
