#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -xe

kubectl -n test get quota,netpol,limitrange
helm -n test list

