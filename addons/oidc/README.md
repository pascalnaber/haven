# Haven - OpenID Connect (oidc)

OIDC is the preferred way to connect to Kubernetes clusters. You can give cluster access to e.g. your Active Directory or Gitlab group users and fine tune with Kubernetes RBAC.

## Prerequisites

Make sure you installed the [Traefik addon](../traefik/README.md).

## Provisioning cluster and deploying dex and the authenticator.

Run `addons/oidc/provision.sh` from inside Haven. The script will guide you through what you have to do to prepare for a deployment. Make sure provided hostnames have their DNS resolve to the ingress controller.

NOTE: There is a hard to solve timing issue when initially provisioning the full OIDC stack. Should the authorization URL not become available (e.g. 503: Service Unavailable) you should check the status of the pods (try `oidc/status.sh`). Usually it helps to delete the dex pod, wait for it to come back and then delete dex-k8s-authenticator.

## Upgrades

See [UPGRADE.md](UPGRADE.md).

## Cleanup

From Haven run `addons/oidc/destroy.sh`.

## License
Copyright © VNG Realisatie 2019-2020
Licensed under the EUPL
