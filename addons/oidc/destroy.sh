#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will DESTROY OpenID Connect stack powered by Dex on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/oidc

if [[ ! -d $DEPLOYPATH ]]; then
    echo "$DEPLOYPATH does not exist, nothing to destroy!"

    exit 1
fi

helm uninstall -n oidc ca
helm uninstall -n oidc dex
helm uninstall -n oidc dex-k8s-authenticator

# Implicitely removes Kubernetes CA configmap.
kubectl delete ns oidc

rm -r $DEPLOYPATH

echo -e "\n\n[Haven] Destroyed OIDC (dex + dex-k8s-authenticator).\n\n"
