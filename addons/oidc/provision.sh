#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will provision OpenID Connect stack powered by Dex on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

OIDC_DEPLOYPATH=$STATEPATH/addons/oidc
TRAEFIK_DEPLOYPATH=$STATEPATH/addons/traefik

if [[ ! -f $TRAEFIK_DEPLOYPATH/traefik-values.yml ]]; then
    echo "[Haven] OIDC depends on the Traefik addon. Install the Traefik addon first."
    exit 1
fi

if [[ ! -d $OIDC_DEPLOYPATH ]]; then
    mkdir -p $OIDC_DEPLOYPATH
fi

if [[ ! -f $OIDC_DEPLOYPATH/auth.host ]]; then
    echo "[Haven] First create $OIDC_DEPLOYPATH/auth.host (see dist/ for an example)."

    exit 1
fi

if [[ ! -f $OIDC_DEPLOYPATH/dex.host ]]; then
    echo "[Haven] First create $OIDC_DEPLOYPATH/dex.host (see dist/ for an example)."

    exit 1
fi

if [[ ! -f $OIDC_DEPLOYPATH/ca.host ]]; then
    echo "[Haven] First create $OIDC_DEPLOYPATH/ca.host (see dist/ for an example)."

    exit 1
fi

# Create ns.
echo "[Haven] Creating namespace 'oidc' if it does not yet exist."

kubectl create ns oidc || echo "Assuming namespace oidc already exists."

# Copy dist files to non-existing deploy files.
echo -e "\n[Haven] Copying oidc distribution files to state directory without overwriting existing files."

false | cp -i /opt/haven/addons/oidc/dist/* $OIDC_DEPLOYPATH/

# Patch config files.
AUTH_HOST=$(cat $OIDC_DEPLOYPATH/auth.host)
DEX_HOST=$(cat $OIDC_DEPLOYPATH/dex.host)
CA_HOST=$(cat $OIDC_DEPLOYPATH/ca.host)
LB_HOST=$(cat /tmp/kubectl.conf | grep server | awk '{ print $2 }')

echo -e "\n\n[Haven] Patching config files with variables."

sed -i "s|%AUTH_HOST%|${AUTH_HOST}|g" $OIDC_DEPLOYPATH/dex-values.yaml
sed -i "s|%DEX_HOST%|${DEX_HOST}|g" $OIDC_DEPLOYPATH/dex-values.yaml

sed -i "s|%CLUSTER%|${CLUSTER}|g" $OIDC_DEPLOYPATH/dex-k8s-authenticator-values.yaml
sed -i "s|%AUTH_HOST%|${AUTH_HOST}|g" $OIDC_DEPLOYPATH/dex-k8s-authenticator-values.yaml
sed -i "s|%DEX_HOST%|${DEX_HOST}|g" $OIDC_DEPLOYPATH/dex-k8s-authenticator-values.yaml
sed -i "s|%CA_HOST%|${CA_HOST}|g" $OIDC_DEPLOYPATH/dex-k8s-authenticator-values.yaml
sed -i "s|%LB_HOST%|${LB_HOST}|g" $OIDC_DEPLOYPATH/dex-k8s-authenticator-values.yaml

# User config.
echo -e "\n[Haven] Open another terminal on your local machine and edit '~/.haven/state/$CLUSTER/addons/oidc/dex-values.yaml' to add your connectors like Active Directory.\n\nPress ENTER when ready...\n\n"
read

# Create k8s-ca.crt configmap prepping for helm ca deployment.
echo "[Haven] Creating k8s-ca.crt configmap."

cat /tmp/kubectl.conf | grep certificate-authority-data | awk '{ print $2 }' | base64 -d > $OIDC_DEPLOYPATH/k8s-ca.crt
echo -ne "apiVersion: v1\nkind: ConfigMap\nmetadata:\n  name: k8s-ca\n  namespace: oidc\ndata:\n  k8s-ca.crt: |\n" > $OIDC_DEPLOYPATH/k8s-ca.yaml
sed -e 's/^/    /' $OIDC_DEPLOYPATH/k8s-ca.crt >> $OIDC_DEPLOYPATH/k8s-ca.yaml

kubectl -n oidc apply -f $OIDC_DEPLOYPATH/k8s-ca.yaml

# Prep CA helm chart.
echo -e "\n[Haven] Packaging helm CA chart."

cp -R /opt/haven/addons/oidc/ca-chart /opt/haven/state/addons/oidc/
cd /opt/haven/state/addons/oidc
mv ca-chart/values.yaml.dist ca-chart/values.yaml
sed -i "s|%CA_HOST%|${CA_HOST}|g" ca-chart/values.yaml
helm package ca-chart
cd /opt/haven

helm repo add commonground https://charts.commonground.nl/
helm repo update

# Deploy helm charts.
setsecret oidc_client_secret $(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c32)

helm install ca --namespace oidc state/addons/oidc/ca-chart-0.1.0.tgz && rm -f state/addons/oidc/ca-chart-0.1.0.tgz
sed "s/%CLIENT_SECRET%/$(getsecret oidc_client_secret)/g" $OIDC_DEPLOYPATH/dex-values.yaml > /tmp/dex-values.yaml && helm install dex --namespace oidc --values /tmp/dex-values.yaml commonground/dex --version ${DEX_CHART_VERSION} && rm /tmp/dex-values.yaml
sed "s/%CLIENT_SECRET%/$(getsecret oidc_client_secret)/g" $OIDC_DEPLOYPATH/dex-k8s-authenticator-values.yaml > /tmp/dex-k8s-authenticator-values.yaml && helm install dex-k8s-authenticator --namespace oidc --values /tmp/dex-k8s-authenticator-values.yaml /opt/haven/addons/oidc/dex-k8s-authenticator/charts/dex-k8s-authenticator && rm -f /tmp/dex-k8s-authenticator-values.yaml

# Set kops cluster config: OIDC params.
kops get cluster -o yaml | \
sed "s|  kubeAPIServer\:|  kubeAPIServer\:\n    oidcIssuerURL\: https://${DEX_HOST}\n    oidcClientID\: kubernetes\n    oidcUsernameClaim\: email\n    oidcUsernamePrefix\: 'oidc:'\n    oidcGroupsClaim\: groups\n    oidcGroupsPrefix\: 'oidc:'|" | \
kops replace -f -

echo -ne "\n\n[Haven] Deployed OpenID Connect stack. Cluster rolling update required. Press ENTER to continue..."
read
echo -ne "[Haven] Running 'kops rolling-update cluster --yes'.\n\n***** YOU MUST RUN 'upgrade-helper' FROM ANOTHER HAVEN TERMINAL RIGHT NOW! *****\n\n"

source $STATEPATH/cluster.env

if [[ "openstack" == $CLOUD ]]; then
    # OpenStack tends to need some more time to validate than 15 minutes.
    kops rolling-update cluster --yes --validation-timeout=25m
else
    kops rolling-update cluster --yes
fi
