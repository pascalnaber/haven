#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will upgrade OpenID Connect stack powered by Dex on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/oidc

if [[ ! -d $DEPLOYPATH ]]; then
    echo "$DEPLOYPATH does not exist, nothing to upgrade!"

    exit 1
fi

helm repo add commonground https://charts.commonground.nl/
helm repo update

helm upgrade ca --namespace oidc $DEPLOYPATH/ca-chart

sed "s/%CLIENT_SECRET%/$(getsecret oidc_client_secret)/g" $DEPLOYPATH/dex-values.yaml > /tmp/dex-values.yaml && \
helm upgrade dex --namespace oidc --values /tmp/dex-values.yaml commonground/dex --version ${DEX_CHART_VERSION} && \
rm -f /tmp/dex-values.yaml

sed "s/%CLIENT_SECRET%/$(getsecret oidc_client_secret)/g" $DEPLOYPATH/dex-k8s-authenticator-values.yaml > /tmp/dex-k8s-authenticator-values.yaml && \
helm upgrade dex-k8s-authenticator --namespace oidc --values /tmp/dex-k8s-authenticator-values.yaml /opt/haven/addons/oidc/dex-k8s-authenticator/charts/dex-k8s-authenticator && \
rm -f /tmp/dex-k8s-authenticator-values.yaml

## Done.
echo -e "\n\n[Haven] Upgraded OpenID Connect stack.\n\n"
