#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

# HELPER SCRIPT. SHOULD NOT BE RUN MANUALLY.

set -e

cp /etc/kubernetes/ssl/ca.crt /home/core/k8s-ca.crt
chown core:core /home/core/k8s-ca.crt
chmod 600 /home/core/k8s-ca.crt
