#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -xe

helm get values gatekeeper
kubectl get validatingwebhookconfigurations.admissionregistration.k8s.io

kubectl get constrainttemplates
kubectl get constraints
