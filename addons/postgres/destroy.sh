#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will DESTROY Postgres Operator and all databases that were deployed using it.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/postgres

if [[ ! -d $DEPLOYPATH ]]; then
    echo "$DEPLOYPATH does not exist, nothing to destroy!"

    exit 1
fi

helm uninstall -n kube-system postgres-operator

rm -r $DEPLOYPATH

echo -e "\n\n[Haven] Destroyed Postgres Operator.\n\n"
