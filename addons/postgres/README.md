# Haven - Postgres

## Postgres Operator

Besides Haven having KubeDB on board, you can also use the more specific [Postgres Operator](https://postgres-operator.readthedocs.io/en/latest/) to provision Postgres databases. It includes a web frontend as well.

From Haven run `addons/postgres/provision.sh`. The script will guide you through what you have to do to prepare for a deployment. Make sure provided hostnames have their DNS resolve to the ingress controller.

## Upgrades

You can upgrade the operator by running `addons/postgres/upgrade.sh`. Version numbers are assumed by the Haven version you are using when running the upgrade.

## Cleanup

From Haven run `addons/postgres/destroy.sh`.

## License
Copyright © VNG Realisatie 2019-2020
Licensed under the EUPL
