#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will provision Postgres Operator by Zalando on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/postgres

if [[ ! -d $DEPLOYPATH ]]; then
    mkdir -p $DEPLOYPATH
fi

# Copy dist files to non-existing deploy files.
echo -e "\n[Haven] Copying Postgres Operator distribution files to state directory without overwriting existing files."

false | cp -i /opt/haven/addons/postgres/dist/* $DEPLOYPATH/

# Install.
echo -e "\n[Haven] Deploying operator chart."

helm install postgres-operator --namespace kube-system /opt/haven/addons/postgres/postgres-operator/charts/postgres-operator -f $DEPLOYPATH/values-operator.yaml

## Done.
echo -e "\n\n[Haven] Deployed Postgres Operator.\n"
