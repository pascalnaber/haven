#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will upgrade Postgres Operator on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/postgres

if [[ ! -d $DEPLOYPATH ]]; then
    echo "$DEPLOYPATH does not exist, nothing to upgrade!"

    exit 1
fi

helm upgrade postgres-operator --namespace kube-system /opt/haven/addons/postgres/postgres-operator/charts/postgres-operator --values $DEPLOYPATH/values-operator.yaml

echo -e "\n\n[Haven] Upgraded Postgres Operator.\n\n"
