# Haven - Monitoring

## Logging with Loki. Metrics with Prometheus. Networking internal overview with Netchecker. Frontend with Grafana.

## Prerequisites

Make sure you installed the [Traefik addon](../traefik/README.md) and [OIDC addon](../oidc/README.md). If you are running metrics-server you can optionally uninstall it because Prometheus will take over.

## Install

From Haven run `addons/monitoring/provision.sh`. The script will guide you through what you have to do to prepare for a deployment (e.g. create a hostname file). Make sure provided hostnames have their DNS resolve to the ingress controller.

Afterwards you can use `addons/monitoring/status.sh` to quickly look into your monitoring namespace.

Grafana will be https exposed using your provided hostname and admin user with a generated password.

## Upgrades

From Haven run `addons/monitoring/upgrade.sh`.

## User management

You can manage users (and much more) using Grafana.

## Alerting

You can manage alerts (and much more) using Grafana.

## Cleanup

From Haven run `addons/monitoring/destroy.sh`.

## License
Copyright © VNG Realisatie 2019-2020
Licensed under the EUPL
