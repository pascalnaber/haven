#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -xe

kubectl -n monitoring get all,ing,pvc
