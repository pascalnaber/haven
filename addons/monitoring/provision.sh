#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will provision Loki, Prometheus, Grafana and Netchecker on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

MONITORING_DEPLOYPATH=$STATEPATH/addons/monitoring
TRAEFIK_DEPLOYPATH=$STATEPATH/addons/traefik
OIDC_DEPLOYPATH=$STATEPATH/addons/oidc

if [[ ! -f $TRAEFIK_DEPLOYPATH/traefik-values.yml ]]; then
    echo "[Haven] Monitoring depends on the Traefik addon. Install the Traefik addon first."
    exit 1
fi

if [[ ! -f $OIDC_DEPLOYPATH/dex-values.yaml ]]; then
    echo "[Haven] Monitoring depends on the OIDC addon. Install the OIDC addon first."
    exit 1
fi

if [[ ! -d $MONITORING_DEPLOYPATH ]]; then
    mkdir -p $MONITORING_DEPLOYPATH
fi

if [[ ! -f $MONITORING_DEPLOYPATH/host.name ]]; then
    echo "[Haven] First create $MONITORING_DEPLOYPATH/host.name (see dist/ for an example)."

    exit 1
fi

# Create ns.
echo "[Haven] Creating namespace 'monitoring' if it does not yet exist."

kubectl create ns monitoring || echo "[Haven] Assuming namespace monitoring already exists."

echo "[Haven] Labeling namespace for network policies."
kubectl label ns monitoring --overwrite networkPolicy=monitoring

# Copy dist files to non-existing deploy files.
echo -e "\n[Haven] Copying distribution files to state directory without overwriting existing files."

false | cp -i /opt/haven/addons/monitoring/dist/* $MONITORING_DEPLOYPATH/

## Prometheus and Grafana.

# Config.
echo -e "\n\n[Haven] Creating secrets."

MONITORING_HOSTNAME=$(cat $MONITORING_DEPLOYPATH/host.name)
DEX_HOSTNAME=$(cat $OIDC_DEPLOYPATH/dex.host)

GRAFANA_PASS=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c20)
setsecret monitoring_grafana_pass $GRAFANA_PASS

GRAFANA_CLIENT_SECRET=$(< /dev/urandom tr -dc a-f0-9 | head -c64)

sed -i "s|%MONITORING_HOSTNAME%|${MONITORING_HOSTNAME}|g" $MONITORING_DEPLOYPATH/values-prometheus.yaml
sed -i "s|%DEX_HOSTNAME%|${DEX_HOSTNAME}|g" $MONITORING_DEPLOYPATH/values-prometheus.yaml
sed -i "s|%GRAFANA_CLIENT_SECRET%|${GRAFANA_CLIENT_SECRET}|g" $MONITORING_DEPLOYPATH/values-prometheus.yaml

sed "s|%GRAFANA_PASS%|${GRAFANA_PASS}|g" $MONITORING_DEPLOYPATH/values-prometheus.yaml > /tmp/values-prometheus.yaml

## Loki. Data source will be picked up automatically by Grafana when Loki has been installed first and is healthy.
# NOTE: Loki-stack can deploy prometheus and grafana all together as well, but not as comprehensive/full featured as kube-prometheus-stack does.
echo -e "\n[Haven] Installing Loki."

helm repo add loki https://grafana.github.io/loki/charts
helm repo update

helm install loki --namespace monitoring loki/loki-stack -f $MONITORING_DEPLOYPATH/values-loki.yaml

# Wait for Loki so Grafana can automatically detect and add it's data source.
while ! kubectl -n monitoring get pods --selector=app=loki | grep "1\/1" >/dev/null; do echo "[Haven] $(date) Waiting 10s for Loki to become healthy"; sleep 10; done

## Prometheus / Grafana.
echo -e "\n\n[Haven] Installing Prometheus and Grafana."

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

helm install prometheus --namespace monitoring prometheus-community/kube-prometheus-stack --set prometheusOperator.createCustomResource=false -f /tmp/values-prometheus.yaml

rm -f /tmp/values-prometheus.yaml

kubectl -n monitoring apply -f $MONITORING_DEPLOYPATH/discover.yaml

## Netchecker.
echo -e "\n\n[Haven] Installing Netchecker."

kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-agent-sa.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-agent-ds.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-agent-hostnet-clusterrole.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-agent-hostnet-clusterrolebinding.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-agent-hostnet-ds.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-agent-hostnet-psp.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-server-sa.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-server-clusterrole.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-server-clusterrolebinding.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-server-deployment.yaml
kubectl apply -f $MONITORING_DEPLOYPATH/netchecker-server-svc.yaml

# Wait for Grafana to become alive and ready.
while ! curl -u "admin:${GRAFANA_PASS}" https://${MONITORING_HOSTNAME}/api/gnet/dashboards/5727 2>&1 | grep -q 5727; do echo "[Haven] $(date) Waiting 10s for Grafana to serve"; sleep 10; done

# Import dashboards.
echo -e "\n\n[Haven] Importing dashboards."

# - Loki quick search.
DASHBOARD=$(curl -u "admin:${GRAFANA_PASS}" https://${MONITORING_HOSTNAME}/api/gnet/dashboards/12019 2>/dev/null | jq .json)
curl -X POST -H "Content-Type: application/json" -u "admin:${GRAFANA_PASS}" -d "{ \"dashboard\": ${DASHBOARD}, \"overwrite\": true, \"inputs\": [{\"name\": \"DS_LOKI\", \"type\": \"datasource\", \"pluginId\": \"loki\", \"value\": \"Loki\"}, {\"name\": \"DS_PROMETHEUS\", \"type\": \"datasource\", \"pluginId\": \"Prometheus\", \"value\": \"Prometheus\"}] }" https://${MONITORING_HOSTNAME}/api/dashboards/import 2>/dev/null
# - Netchecker.
DASHBOARD=$(curl -u "admin:${GRAFANA_PASS}" https://${MONITORING_HOSTNAME}/api/gnet/dashboards/5727 2>/dev/null | jq .json)
curl -X POST -H "Content-Type: application/json" -u "admin:${GRAFANA_PASS}" -d "{ \"dashboard\": ${DASHBOARD}, \"overwrite\": true, \"inputs\": [{\"name\": \"DS_PROMETHEUS\", \"type\": \"datasource\", \"pluginId\": \"prometheus\", \"value\": \"Prometheus\"}] }" https://${MONITORING_HOSTNAME}/api/dashboards/import 2>/dev/null

# Star some dashboards for admin user.
echo -e "\n\n[Haven] Starring dashboards."

# - Kubernetes cluster.
ID=$(curl -H "Content-Type: application/json" -u "admin:${GRAFANA_PASS}" https://${MONITORING_HOSTNAME}/api/dashboards/uid/efa86fd1d0c121a26444b636a3f509a8 2>/dev/null | jq '.dashboard.id')
curl -X POST -H "Content-Type: application/json" -u "admin:${GRAFANA_PASS}" https://${MONITORING_HOSTNAME}/api/user/stars/dashboard/${ID} 2>/dev/null
# - Loki quick search.
ID=$(curl -H "Content-Type: application/json" -u "admin:${GRAFANA_PASS}" https://${MONITORING_HOSTNAME}/api/dashboards/uid/liz0yRCZz 2>/dev/null | jq '.dashboard.id')
curl -X POST -H "Content-Type: application/json" -u "admin:${GRAFANA_PASS}" https://${MONITORING_HOSTNAME}/api/user/stars/dashboard/${ID} 2>/dev/null
# - Netchecker.
ID=$(curl -H "Content-Type: application/json" -u "admin:${GRAFANA_PASS}" https://${MONITORING_HOSTNAME}/api/dashboards/uid/zn2oeyZiz 2>/dev/null | jq '.dashboard.id')
curl -X POST -H "Content-Type: application/json" -u "admin:${GRAFANA_PASS}" https://${MONITORING_HOSTNAME}/api/user/stars/dashboard/${ID} 2>/dev/null

# OIDC: Update Dex.
echo -e "\n\n[Haven] Adding Grafana to OIDC clients. Please confirm the upgrade."

sed -i "s|  staticClients\:|  staticClients\:\n  - id\: grafana\n    redirectURIs\:\n    - 'https://${MONITORING_HOSTNAME}/login/generic_oauth'\n    name\: 'grafana'\n    secret\: ${GRAFANA_CLIENT_SECRET}|g" $OIDC_DEPLOYPATH/dex-values.yaml

bash /opt/haven/addons/oidc/upgrade.sh

## Done.
echo -e "\n[Haven] ** Grafana admin password: $GRAFANA_PASS **"

echo -e "\n[Haven] Deployed Loki, Prometheus, Grafana and Netchecker.\n"
