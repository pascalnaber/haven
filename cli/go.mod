module gitlab.com/commonground/haven/haven/cli

go 1.13

require (
	github.com/AlecAivazis/survey/v2 v2.2.0
	github.com/Masterminds/semver/v3 v3.1.0
	github.com/googleapis/gnostic v0.3.1 // indirect
	github.com/gookit/color v1.3.2
	github.com/jawher/mow.cli v1.2.0
	github.com/markbates/pkger v0.17.1
	github.com/olekukonko/tablewriter v0.0.4
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	helm.sh/helm/v3 v3.3.4
	k8s.io/api v0.18.8
	k8s.io/apimachinery v0.18.8
	k8s.io/cli-runtime v0.18.8
	k8s.io/client-go v0.18.8
	sigs.k8s.io/yaml v1.2.0
)

replace (
	github.com/Azure/go-autorest => github.com/Azure/go-autorest v13.3.2+incompatible
	github.com/docker/distribution => github.com/docker/distribution v0.0.0-20191216044856-a8371794149d
)
