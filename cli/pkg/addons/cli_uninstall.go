// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package addons

import (
	"fmt"

	cli "github.com/jawher/mow.cli"
	"gitlab.com/commonground/haven/haven/cli/pkg/helm"
)

func cmdUninstall(cmd *cli.Cmd) {
	name := cmd.StringArg("NAME", "", "The addon to install")

	cmd.Action = func() {
		addon, err := Get(*name)
		if err != nil {
			fmt.Printf("Error while getting addon: %s\n", err)
			return
		}

		helmClient, err := helm.NewClient(addon.Namespace)
		if err != nil {
			fmt.Printf("Error constructing client: %s\n", err)
			return
		}

		for _, release := range addon.Releases {
			fmt.Printf("Uninstalling %s\n", release.Name)

			_, err := helmClient.Uninstall(release.Name)
			if err != nil {
				fmt.Printf("Error while uninstalling: %s\n", err)
			}
		}

		fmt.Printf("Uninstalled addon %s\n", *name)
	}
}
