// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package ssh

import (
	"fmt"
	"os/exec"
	"strings"
)

// Run a specific command on an external host through SSH and
// return the combined stdout and stderr output or an error.
// Uses shell calls instead of a go ssh client, because of the complexity
// certain ssh configs may bring (ProxyJump(s), IdentityFile(s), etc).
func Run(host string, command string) (string, error) {
	s := strings.Split(fmt.Sprintf("ssh %s ''%s''", host, command), " ")

	cmd := exec.Command(s[0], s[1:]...)

	out, err := cmd.CombinedOutput()

	if err != nil {
		return "", err
	}

	return string(out), nil
}
