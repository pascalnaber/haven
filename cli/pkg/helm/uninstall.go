// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package helm

import (
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/release"
)

// Uninstall a specific Helm release
func (h *Client) Uninstall(releaseName string) (*release.UninstallReleaseResponse, error) {
	client := action.NewUninstall(h.actionConfig)
	result, err := client.Run(releaseName)
	if err != nil {
		return nil, err
	}

	return result, err
}
