// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package helm

import (
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/release"
)

// Install a specific Helm chart
func (h *Client) Install(releaseName string, chartName string, values map[string]interface{}) (*release.Release, error) {
	client := action.NewInstall(h.actionConfig)
	client.ReleaseName = releaseName
	client.Namespace = h.namespace
	client.CreateNamespace = true

	chartPath, err := client.ChartPathOptions.LocateChart(chartName, settings)
	if err != nil {
		return nil, err
	}

	chartRequested, err := loader.Load(chartPath)
	if err != nil {
		return nil, err
	}

	fetchedDependencies, err := fetchChartDependencies(chartRequested, chartPath, client.ChartPathOptions.Keyring)
	if err != nil {
		return nil, err
	}

	if fetchedDependencies {
		// Reload the chart with the updated Chart.lock file.
		chartRequested, err = loader.Load(chartPath)
		if err != nil {
			return nil, err
		}
	}

	return client.Run(chartRequested, values)
}
