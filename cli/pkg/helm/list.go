// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package helm

import (
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/release"
)

// List installed Helm charts
func (h *Client) List() ([]*release.Release, error) {
	client := action.NewList(h.actionConfig)
	return client.Run()
}
