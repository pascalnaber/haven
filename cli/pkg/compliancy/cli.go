// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	cli "github.com/jawher/mow.cli"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var (
	kubeLatest = "undefined"
)

// CLIConfig renders the command line function
var CLIConfig = func(config *cli.Cmd) {
	sshHost := config.StringOpt("ssh-host", "", "SSH host name of a master node")
	runCNCFChecks := config.BoolOpt("cncf", false, "Run external CNCF checks")

	config.Action = func() {
		cmdCheck(sshHost, runCNCFChecks)
	}
}

var (
	sshConfigured  bool = false
	cncfConfigured bool = false
)

// cmdChecks validates requirements, runs the in scope checks and outputs
// results.
func cmdCheck(sshHost *string, runCNCFChecks *bool) {
	kubeConfig, err := connectK8s()
	if err != nil {
		fmt.Printf("[!] Fatal: %s\n", err.Error())
		return
	}

	if "" != *sshHost {
		err := connectSsh(sshHost)
		if err != nil {
			fmt.Printf("[!] Fatal: %s", err)
			return
		}

		sshConfigured = true
		fmt.Printf("[+] SSH: configured host '%s', enabled checks requiring SSH access.\n", *sshHost)
	}

	if !sshConfigured {
		fmt.Println("[-] SSH not configured! Skipping checks requiring SSH access.")
	}

	if *runCNCFChecks {
		if path, err := exec.LookPath("sonobuoy"); err == nil {
			cncfConfigured = true

			fmt.Printf("[+] Sonobuoy: found binary at '%s', enabled external CNCF check.\n", path)
		} else {
			fmt.Printf("[-] Sonobuoy binary not found! Skipping external CNCF check.\n")
		}
	} else {
		fmt.Println("[-] CNCF set to false! Skipping external CNCF check.")
	}

	fmt.Println("[I] Running checks...")

	checker, err := NewChecker(kubeConfig, kubeLatest, *sshHost, *runCNCFChecks)
	if err != nil {
		fmt.Printf("[!] Error occured while starting checker: %s", err)
		return
	}

	compliancyResults, suggestedResults, err := checker.Run()
	if err != nil {
		fmt.Printf("[!] Error occured while checking the cluster: %s", err)
		return
	}

	checker.PrintResults(CompliancyChecks, compliancyResults, true)
	checker.PrintResults(SuggestedChecks, suggestedResults, false)
}

// connectK8s attempts to connect to a Kubernetes cluster via kube config or
// in cluster.
func connectK8s() (*rest.Config, error) {
	var cfg *rest.Config
	var err error
	var path string

	path = os.Getenv("KUBECONFIG")
	if "" == path {
		var home = os.Getenv("HOME")
		if "" == home {
			home = os.Getenv("USERPROFILE")
		}

		path = filepath.Join(home, ".kube", "config")
	}

	if _, err = os.Stat(path); err == nil {
		cfg, err = clientcmd.BuildConfigFromFlags("", path)
		if err != nil {
			return nil, err
		}

		fmt.Printf("[+] Kubernetes connection using KUBECONFIG: %s.\n", path)
	}

	if nil == cfg {
		cfg, err = rest.InClusterConfig()
		if err != nil {
			return nil, err
		}

		fmt.Println("[+] Kubernetes connection using In Cluster config.\n")
	}

	return cfg, nil
}

// connectSsh attempts to connect to the given node and assert this works.
func connectSsh(sshHost *string) error {
	c := strings.Split(fmt.Sprintf("ssh %s ''%s''", *sshHost, "uname -a"), " ")
	cmd := exec.Command(c[0], c[1:]...)

	cfg := `
ForwardAgent no
ServerAliveInterval 120

Host bastion
  User core
  IdentityFile ~/tmp/ssh_id_rsa
  Hostname 11.22.33.44

Host 10.0.101.*
  User core
  IdentityFile ~/tmp/ssh_id_rsa
  ProxyJump bastion
				`

	if err := cmd.Run(); err != nil {
		return errors.New(fmt.Sprintf("SSH: configured host '%s', but connection failed: '%s'\n\nTry to configure ~/.ssh/config like this:\n%s\n\n", *sshHost, err.Error(), cfg))
	}

	return nil
}
