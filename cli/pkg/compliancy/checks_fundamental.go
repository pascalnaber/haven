// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"errors"

	authorizationv1 "k8s.io/api/authorization/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func fundamentalSelfTest(config *Config) (Result, error) {
	sar := &authorizationv1.SelfSubjectAccessReview{
		Spec: authorizationv1.SelfSubjectAccessReviewSpec{
			NonResourceAttributes: &authorizationv1.NonResourceAttributes{
				Verb: "*",
				Path: "*",
			},
		},
	}

	response, err := config.KubeClient.AuthorizationV1().SelfSubjectAccessReviews().Create(context.Background(), sar, metav1.CreateOptions{})

	if err != nil {
		return ResultNo, err
	}

	// This check is fundamental. If it fails execution must stop.
	if !response.Status.Allowed {
		return ResultNo, errors.New("HCC does not have cluster-admin access.")
	}

	return ResultYes, nil
}
