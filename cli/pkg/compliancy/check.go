// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

// Check represents a single compliancy check
type Check struct {
	Category    Category
	Name        string
	SSHRequired bool
	f           func(*Config) (Result, error)
}
