// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

// CompliancyChecks is a list that contains all compliancy checks. Haven Compliancy is accomplished by passing all of these checks together.
var CompliancyChecks = []Check{
	// Fundamental checks are required to pass and will immediately abort the HCC run upon failure.
	Check{Category: CategoryFundamental, Name: "Self test: does HCC have cluster-admin", SSHRequired: false, f: fundamentalSelfTest},

	// Infra checks ensure the underlying infrastructe has been set up according to best practices.
	Check{Category: CategoryInfrastructure, Name: "Multiple availability zones in use", SSHRequired: false, f: infraMultiAZ},
	Check{Category: CategoryInfrastructure, Name: "Running at least 3 master nodes", SSHRequired: false, f: infraMultiMaster},
	Check{Category: CategoryInfrastructure, Name: "Running at least 3 worker nodes", SSHRequired: false, f: infraMultiWorker},
	Check{Category: CategoryInfrastructure, Name: "Nodes have SELinux, Grsecurity, AppArmor or LKRG enabled", SSHRequired: true, f: infraSecuredNodes},
	Check{Category: CategoryInfrastructure, Name: "Private networking topology", SSHRequired: false, f: infraPrivateTopology},

	// Cluster checks look into Kubernetes cluster configuration.
	Check{Category: CategoryCluster, Name: "Kubernetes version is latest stable or max 2 minor versions behind", SSHRequired: false, f: clusterVersion},
	Check{Category: CategoryCluster, Name: "Role Based Access Control is enabled", SSHRequired: false, f: clusterRBAC},
	Check{Category: CategoryCluster, Name: "Basic auth is disabled", SSHRequired: true, f: clusterBasicAuth},
	Check{Category: CategoryCluster, Name: "ReadWriteMany persistent volumes support", SSHRequired: false, f: clusterVolumes},
	Check{Category: CategoryCluster, Name: "LoadBalancer service type support", SSHRequired: false, f: clusterLoadBalancerService},

	// External checks invoke non-HCC tests to be included in the HCC results.
	Check{Category: CategoryExternal, Name: "CNCF Kubernetes Conformance", SSHRequired: false, f: externalCNCF},

	// Deployment checks look into provisioning of supportive tooling in Kubernetes.
	Check{Category: CategoryDeployment, Name: "Automated HTTPS certificate provisioning", SSHRequired: false, f: deploymentHttpsCerts},
	Check{Category: CategoryDeployment, Name: "Log aggregation is running", SSHRequired: false, f: deploymentLogAggregation},
	Check{Category: CategoryDeployment, Name: "Metrics-server is running", SSHRequired: false, f: deploymentMetricsServer},
}

// SuggestedChecks are merely suggestions and don't influence Haven Compliancy in any way.
var SuggestedChecks = []Check{
	Check{Category: CategorySuggestion, Name: "Haven Dashboard is provisioned", SSHRequired: false, f: suggestionHavenDashboard},
}
