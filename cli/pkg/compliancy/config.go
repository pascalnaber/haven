// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

import (
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// Config holds the configuration of the checker
type Config struct {
	KubeConfig     *rest.Config
	KubeClient     *kubernetes.Clientset
	KubeLatest     string
	SSHHost        string
	CNCFConfigured bool
	HostPlatform   Platform
}
