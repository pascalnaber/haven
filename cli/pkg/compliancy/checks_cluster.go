// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"sort"
	"strings"
	"time"

	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/commonground/haven/haven/cli/pkg/ssh"
)

func clusterVersion(config *Config) (Result, error) {
	// Requires build time version injection.
	if config.KubeLatest == "undefined" {
		return ResultSkipped, nil
	}

	serverVersion, err := config.KubeClient.Discovery().ServerVersion()
	if err != nil {
		return ResultNo, err
	}

	sv, err := semver.NewVersion(serverVersion.String())
	if err != nil {
		return ResultNo, errors.New(fmt.Sprintf("Kubernetes server version: %s, Error: %s", serverVersion.String(), err.Error()))
	}

	lv, err := semver.NewVersion(config.KubeLatest)
	if err != nil {
		return ResultNo, errors.New(fmt.Sprintf("Kubernetes latest version: %s, Error: %s", config.KubeLatest, err.Error()))
	}

	fmt.Printf("[I] Latest stable Kubernetes release: %s.\n", config.KubeLatest)

	maxTwoMinorBehind, _ := semver.NewConstraint(fmt.Sprintf(">= %d.%d", lv.Major(), lv.Minor()))

	*sv = sv.IncMinor().IncMinor()

	if maxTwoMinorBehind.Check(sv) {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func clusterRBAC(config *Config) (Result, error) {
	groupList, err := config.KubeClient.DiscoveryClient.ServerGroups()
	if err != nil {
		return ResultNo, err
	}

	apiVersions := metav1.ExtractGroupVersions(groupList)

	sort.Strings(apiVersions)

	for _, v := range apiVersions {
		if "rbac.authorization.k8s.io/v1" == v {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}

func clusterBasicAuth(config *Config) (Result, error) {
	out, err := ssh.Run(config.SSHHost, "ps uax | grep kube-apiserver")

	if err != nil {
		return ResultNo, err
	}

	if !strings.Contains(out, "--basic-auth-file") {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func clusterVolumes(config *Config) (Result, error) {
	// ReadWriteMany as listed on https://kubernetes.io/docs/concepts/storage/storage-classes/
	// Possibly more at https://github.com/kubernetes-incubator/external-storage
	provisioners := [...]string{
		"ceph",
		"nfs",
		"manila",
		"efs",
		"azure-file",
		"glusterfs",
		"quobyte",
		"portworx-volume",
	}

	sc, err := config.KubeClient.StorageV1().StorageClasses().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	// If a known provisioner is found we're done quickly.
	for _, s := range sc.Items {
		for _, p := range provisioners {
			if strings.Contains(strings.ToUpper(s.Provisioner), strings.ToUpper(p)) {
				return ResultYes, nil
			}
		}
	}

	// Fallback: try to create a RWX volume with each storageclass.
	app := "hcc-test-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte("Haven Compliancy Checker"))[:5])
	ns := "default"

	for _, s := range sc.Items {
		fmt.Printf("[I] Check ReadWriteMany: Using fallback method with \"%s\" storage class for 1-3 minutes.\n", s.Name)

		if _, err := config.KubeClient.CoreV1().PersistentVolumeClaims(ns).Create(
			context.Background(),
			&apiv1.PersistentVolumeClaim{
				ObjectMeta: metav1.ObjectMeta{
					Name: app,
				},
				Spec: apiv1.PersistentVolumeClaimSpec{
					StorageClassName: &s.Name,
					AccessModes:      []apiv1.PersistentVolumeAccessMode{apiv1.ReadWriteMany},
					Resources: apiv1.ResourceRequirements{
						Requests: apiv1.ResourceList{
							apiv1.ResourceName(apiv1.ResourceStorage): resource.MustParse("1Gi"),
						},
					},
				},
			},
			metav1.CreateOptions{},
		); err != nil {
			return ResultNo, err
		}

		defer func() {
			config.KubeClient.CoreV1().PersistentVolumeClaims(ns).Delete(context.Background(), app, metav1.DeleteOptions{})
		}()

		// We should expect a PVC to be created within 60 seconds.
		start := time.Now()
		for ts := range time.Tick(5 * time.Second) {
			if ts.Sub(start).Seconds() >= 60 {
				return ResultNo, nil
			}

			pvc, err := config.KubeClient.CoreV1().PersistentVolumeClaims(ns).Get(context.Background(), app, metav1.GetOptions{})

			if err != nil {
				return ResultNo, err
			}

			if "Bound" == pvc.Status.Phase {
				break
			}
		}

		replicas := int32(2)

		if _, err := config.KubeClient.AppsV1().Deployments(ns).Create(
			context.Background(),
			&appsv1.Deployment{
				ObjectMeta: metav1.ObjectMeta{
					Name: app,
				},
				Spec: appsv1.DeploymentSpec{
					Replicas: &replicas,
					Selector: &metav1.LabelSelector{
						MatchLabels: map[string]string{
							"app": app,
						},
					},
					Template: apiv1.PodTemplateSpec{
						ObjectMeta: metav1.ObjectMeta{
							Labels: map[string]string{
								"app": app,
							},
						},
						Spec: apiv1.PodSpec{
							Containers: []apiv1.Container{
								{
									Name:  "web",
									Image: "nginx:latest",
									VolumeMounts: []apiv1.VolumeMount{
										{
											Name:      "storage",
											MountPath: "/storage",
										},
									},
								},
							},
							Volumes: []apiv1.Volume{
								{
									Name: "storage",
									VolumeSource: apiv1.VolumeSource{
										PersistentVolumeClaim: &apiv1.PersistentVolumeClaimVolumeSource{
											ClaimName: app,
											ReadOnly:  false,
										},
									},
								},
							},
						},
					},
				},
			},
			metav1.CreateOptions{},
		); err != nil {
			return ResultNo, err
		}

		defer func() {
			config.KubeClient.AppsV1().Deployments(ns).Delete(context.Background(), app, metav1.DeleteOptions{})
		}()

		// We should expect 2 pods both running having the RWM PVC attached within 120 seconds.
		start = time.Now()
		for ts := range time.Tick(5 * time.Second) {
			if ts.Sub(start).Seconds() >= 120 {
				return ResultNo, nil
			}

			pods, err := config.KubeClient.CoreV1().Pods(ns).List(context.Background(), metav1.ListOptions{LabelSelector: "app=" + app})

			if err != nil {
				return ResultNo, err
			}

			attached := 0
			for _, pod := range pods.Items {
				if "Running" == pod.Status.Phase {
					attached += 1

					if 2 == attached {
						return ResultYes, nil
					}
				}
			}
		}
	}

	return ResultNo, nil
}

func clusterLoadBalancerService(config *Config) (Result, error) {
	// Amazon EKS, Azure AKS and Google cloud are known to support on demand loadbalancer creation.
	if config.HostPlatform == PlatformEKS {
		return ResultYes, nil
	}

	if config.HostPlatform == PlatformAKS {
		return ResultYes, nil
	}

	if config.HostPlatform == PlatformGKE {
		return ResultYes, nil
	}

	// Fallback: try to create a LoadBalancer service.
	fmt.Println("[I] Check LoadBalancer service type: Using fallback method for maximum 5 minutes.")

	app := "hcc-test-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte("Haven Compliancy Checker"))[:5])
	ns := "default"

	if _, err := config.KubeClient.CoreV1().Services(ns).Create(
		context.Background(),
		&apiv1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name: app,
				Labels: map[string]string{
					"app": app,
				},
			},
			Spec: apiv1.ServiceSpec{
				Type: apiv1.ServiceTypeLoadBalancer,
				Ports: []apiv1.ServicePort{
					{
						Name: app,
						Port: 42000,
					},
				},
			},
		},
		metav1.CreateOptions{},
	); err != nil {
		return ResultNo, err
	}

	defer func() {
		config.KubeClient.CoreV1().Services(ns).Delete(context.Background(), app, metav1.DeleteOptions{})
	}()

	// We should expect an external (from Kubernetes perspective) ip to be assigned to our loadbalancer service.
	start := time.Now()
	for ts := range time.Tick(5 * time.Second) {
		if ts.Sub(start).Seconds() >= 300 {
			return ResultNo, nil
		}

		service, err := config.KubeClient.CoreV1().Services(ns).Get(context.Background(), app, metav1.GetOptions{})
		if err != nil {
			return ResultNo, err
		}

		if len(service.Status.LoadBalancer.Ingress) > 0 {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}
