// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

import (
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

func externalCNCF(config *Config) (Result, error) {
	fmt.Println("[I] Check CNCF: Running external CNCF checker. This usually takes about 1.5 hours. See: https://github.com/vmware-tanzu/sonobuoy.")

	// Run.
	cmd := strings.Fields("sonobuoy run --mode=certified-conformance")
	runner := exec.Command(cmd[0], cmd[1:]...)
	out, err := runner.CombinedOutput()

	if err != nil {
		if !strings.Contains(string(out), "namespace already exists") {
			return ResultNo, fmt.Errorf("\n\ncommand: `%s`\n\nstdout:\n%s\nstderr:\n%s\n", strings.Join(cmd, " "), out, err)
		}

		fmt.Println("[I] Check CNCF: Sonobuoy namespace already exists: resuming check.")
	}

	// Intercept.
	ctrlc := false

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)

	go func() {
		<-sigs

		fmt.Println("\n\n** User break: aborting **\n\n")

		ctrlc = true
	}()

	// Query.
	cmd = strings.Fields("sonobuoy retrieve")

	for {
		if ctrlc {
			break
		}

		runner = exec.Command(cmd[0], cmd[1:]...)
		out, err = runner.CombinedOutput()

		if err == nil {
			break
		}

		seconds := 0
		for {
			if ctrlc {
				break
			}

			seconds += 1

			if 1 == seconds {
				fmt.Println("[I] Check CNCF: Sleeping for 10 minutes before checking progress again.")
			}

			if 600 == seconds {
				seconds = 0

				break
			}

			time.Sleep(time.Second)
		}
	}

	results := "Status: failed"

	// Process.
	if !ctrlc {
		outputfile := string(out)

		cmd = strings.Fields(fmt.Sprintf("sonobuoy results %s", outputfile))
		runner = exec.Command(cmd[0], cmd[1:]...)
		out, err = runner.CombinedOutput()

		if err != nil {
			return ResultNo, fmt.Errorf("\n\ncommand: `%s`\n\nstdout:\n%s\nstderr:\n%s\n", strings.Join(cmd, " "), out, err)
		}

		results = string(out)
	}

	// Cleanup.
	fmt.Println("[I] Check CNCF: Cleaning up sonobuoy deployment.")

	cmd = strings.Fields("sonobuoy delete --all")
	runner = exec.Command(cmd[0], cmd[1:]...)
	out, err = runner.CombinedOutput()

	if err != nil {
		return ResultNo, fmt.Errorf("\n\ncommand: `%s`\n\nstdout:\n%s\nstderr:\n%s\n", strings.Join(cmd, " "), out, err)
	}

	if !strings.Contains(results, "Status: failed") {
		return ResultYes, nil
	}

	return ResultNo, nil
}
