// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/gookit/color"
	"github.com/olekukonko/tablewriter"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// Checker runs the compliancy checks and returns a list of results
type Checker struct {
	config *Config
}

type Platform int

const (
	PlatformUnknown Platform = iota
	PlatformEKS              // Amazon AWS EKS
	PlatformAKS              // Microsoft Azure AKS
	PlatformGKE              // Google Cloud Platform GKE
)

// NewChecker returns a new Checker
func NewChecker(kubeConfig *rest.Config, kubeLatest string, sshHost string, cncfConfigured bool) (*Checker, error) {
	kubeClient, err := kubernetes.NewForConfig(kubeConfig)
	if err != nil {
		return nil, err
	}

	platform := PlatformUnknown

	awsMasterPattern := regexp.MustCompile(`https:\/\/.*.eks.amazonaws.com`)
	if awsMasterPattern.MatchString(kubeConfig.Host) {
		platform = PlatformEKS
	}

	azureMasterPattern := regexp.MustCompile(`https:\/\/.*.azmk8s.io`)
	if azureMasterPattern.MatchString(kubeConfig.Host) {
		platform = PlatformAKS
	}

	serverVersion, err := kubeClient.DiscoveryClient.ServerVersion()
	googleMasterVersionPattern := regexp.MustCompile(`gke`)
	if googleMasterVersionPattern.MatchString(serverVersion.String()) {
		platform = PlatformGKE
	}

	return &Checker{
		config: &Config{
			KubeConfig:     kubeConfig,
			KubeClient:     kubeClient,
			KubeLatest:     kubeLatest,
			SSHHost:        sshHost,
			CNCFConfigured: cncfConfigured,
			HostPlatform:   platform,
		},
	}, nil
}

// Run the checker and return a slice of results
func (c *Checker) Run() ([]Result, []Result, error) {
	var (
		compliancyResults []Result
		suggestedResults  []Result
	)

	for _, compliancyCheck := range CompliancyChecks {
		if CheckInheritsEnv(compliancyCheck, *c.config) {
			compliancyResults = append(compliancyResults, ResultYes)
			continue
		}

		if c.config.SSHHost == "" && compliancyCheck.SSHRequired {
			compliancyResults = append(compliancyResults, ResultSkipped)
			continue
		}

		if !c.config.CNCFConfigured && compliancyCheck.Name == "CNCF Kubernetes Conformance" {
			compliancyResults = append(compliancyResults, ResultSkipped)
			continue
		}

		compliancyResult, err := compliancyCheck.f(c.config)
		if err != nil {
			return nil, nil, err
		}

		compliancyResults = append(compliancyResults, compliancyResult)
	}

	for _, suggestedCheck := range SuggestedChecks {
		suggestedResult, err := suggestedCheck.f(c.config)
		if err != nil {
			return nil, nil, err
		}

		suggestedResults = append(suggestedResults, suggestedResult)
	}

	return compliancyResults, suggestedResults, nil
}

// PrintResults prints the table of checks with the corresponding results
func (c *Checker) PrintResults(checks []Check, results []Result, summary bool) {
	tableOut := &strings.Builder{}
	table := tablewriter.NewWriter(tableOut)

	table.SetHeader([]string{"Category", "Name", "Passed"})
	table.SetAutoWrapText(false)

	totalChecks := 0
	unknownChecks := 0
	skippedChecks := 0
	failedChecks := 0
	passedChecks := 0

	for i, check := range checks {
		totalChecks++

		if results[i] == ResultUnknown {
			unknownChecks++
		}

		if results[i] == ResultSkipped {
			skippedChecks++
		}

		if results[i] == ResultNo {
			failedChecks++
		}

		if results[i] == ResultYes {
			passedChecks++
		}

		table.Append([]string{string(check.Category), check.Name, string(results[i])})
	}

	table.Render()

	if summary {
		compliancy := fmt.Sprintf("[-] Results: %d out of %d checks passed, %d checks skipped, %d checks unknown.", passedChecks, totalChecks, skippedChecks, unknownChecks)

		if passedChecks == totalChecks {
			color.Green.Printf("%s This is a Haven Compliant cluster\n\n", compliancy)
		} else if failedChecks > 0 {
			color.Red.Printf("%s This is NOT a Haven Compliant cluster\n\n", compliancy)
		} else {
			color.Yellow.Printf("%s This COULD be a Haven Compliant cluster\n\n", compliancy)
		}
	} else {
		fmt.Printf("\nBesides Haven Compliancy these suggestions may improve the overall experience\n\n")
	}

	fmt.Println(tableOut)
}

// CheckInheritsEnv returns yes when a given check should implicitely pass given the underlying host platform.
// Explicit checking is preferred: only use whitelisting when there is no factual possibility to run a certain check,
// like not having SSH access to a managed control plane (there are no master instances).
func CheckInheritsEnv(check Check, config Config) bool {
	if check.Name == "Nodes have SELinux, Grsecurity, AppArmor or LKRG enabled" {
		// AKS uses Ubuntu images having AppArmor enabled. This cannot be checked explicitely because there is no SSH access.
		if config.HostPlatform == PlatformAKS {
			return true
		}
	}

	if check.Name == "Anonymous auth is disabled" {
		// AKS managed control plane is confirmed to have anonymous auth disabled.
		if config.HostPlatform == PlatformAKS {
			return true
		}
	}

	if check.Name == "Basic auth is disabled" {
		// AKS managed control plane is confirmed to have basic auth disabled.
		if config.HostPlatform == PlatformAKS {
			return true
		}
	}

	if check.Name == "Insecure API port is disabled" {
		// AKS managed control plane is confirmed to have the insecure API port disabled.
		if config.HostPlatform == PlatformAKS {
			return true
		}
	}

	return false
}
