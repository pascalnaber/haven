// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

import (
	"context"

	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func suggestionHavenDashboard(config *Config) (Result, error) {
	list, err := config.KubeClient.AppsV1().Deployments(apiv1.NamespaceAll).List(context.Background(), metav1.ListOptions{})

	if err != nil {
		return ResultNo, err
	}

	for _, d := range list.Items {
		if "haven-dashboard" == d.Name {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}
