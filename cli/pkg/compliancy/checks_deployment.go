// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"strings"

	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func deploymentLogAggregation(config *Config) (Result, error) {
	loggers := []string{
		"promtail",
		"fluentd",
		"logstash",
		"filebeat",
		"splunk",
		"logentries",
		"logspout", // One of the recommended papertrail k8s deployments.
		"logagent",
		"collectd",
		"statsd",
		"syslog-ng",
	}

	pods, err := config.KubeClient.CoreV1().Pods(apiv1.NamespaceAll).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, p := range pods.Items {
		for _, l := range loggers {
			if strings.Contains(p.Name, l) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

func deploymentMetricsServer(config *Config) (Result, error) {
	metrics := []string{
		"metrics-server",
		"prometheus",
	}

	pods, err := config.KubeClient.CoreV1().Pods(apiv1.NamespaceAll).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, p := range pods.Items {
		for _, l := range metrics {
			if strings.Contains(p.Name, l) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

func deploymentHttpsCerts(config *Config) (Result, error) {
	providers := []string{
		"traefik",
		"cert-manager",
		"voyager",             // HAProxy with LetsEncrypt.
		"service-ca-operator", // OpenShift.
	}

	pods, err := config.KubeClient.CoreV1().Pods(apiv1.NamespaceAll).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, p := range pods.Items {
		for _, l := range providers {
			if strings.Contains(p.Name, l) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}
