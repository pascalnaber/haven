# Haven CLI

The Haven CLI can be used to validate an existing cluster on Haven compliancy.

## Usage

See `haven --help`.

You can opt in to checks requiring SSH access with --ssh-host <internal-ip-of-any-master>. SSH checks are required for Haven Compliancy.
You can opt out of external CNCF checks which take a long time (1 - 2 hours) with `haven check --cncf=false`. CNCF checks are required for Haven Compliancy.

Haven Compliancy thus requires your final command to end up like this: `haven check --ssh-host <internal-ip-of-any-master>`.

## License
Copyright © VNG Realisatie 2019-2020
Licensed under the EUPL
