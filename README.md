# Haven
Standardized infrastructure for running and managing applications.

## Kubernetes

See [kops/README.md](kops/README.md) for instructions on how to deploy Haven to your cloud provider ("Referentie Implementatie").
See [cli/README.md](cli/README.md) for the Haven CLI tooling. This can be used to determine the compliancy of an existing cluster.

Notes:
- Haven versions up to v2.x used Kubespray instead of Kops as the "Referentie Implementatie" base. Unfortunately Kubespray did not comply with our needs.
- In order to upgrade an old Kubespray Haven cluster to Haven v3.x you'll have to create a new cluster and migrate your workload. This is a one-off migration.

## Addons

Haven ships with many addons you can easily deploy on your cluster to make access and management easier. Make sure you configure the templates where appropriate. For example requests and limits haven been set by benchmarking load on our own clusters with 3 worker nodes each having 4 CPU cores and 8gb RAM.

### Addon: Haven Dashboard

See [addons/haven-dashboard/README.md](addons/haven-dashboard/README.md) for instructions on how to deploy the Haven Dashboard.

### Addon: Kubernetes dashboard

See [addons/kubernetes-dashboard/README.md](addons/kubernetes-dashboard/README.md) for instructions on how to deploy the Kubernetes dashboard.

### Addon: Traefik

See [addons/traefik/README.md](addons/traefik/README.md) for instructions on how to deploy a Traefik ingress controller and cert-manager for automated SSL certificate management.

### Addon: Monitoring

See [addons/monitoring/README.md](addons/monitoring/README.md) for instructions on how to deploy monitoring with Loki, Prometheus, Netchecker and Grafana.

### Addon: OpenID Connect

See [addons/oidc/README.md](addons/oidc/README.md) for instructions on how to deploy OpenID Connect powered by Dex, allowing for Active Directory/Github etc. authentication.

### Addon: Postgres

See [addons/postgres/README.md](addons/postgres/README.md) for instructions on how to deploy Postgres Operator by Zalando, allowing for proper Postgres HA cluster deployments.

## Devstack

See [devstack/README.md](devstack/README.md) for instructions on how to set up your own local OpenStack environment.

## Release management

During Haven coding/ engineering you can set haven.sh VERSION to latest.

When ready to release a new Haven version (for example "v3.0.0"):

- Make sure to use semantic versioning correctly (Major.Minor.Patch)
- Update VERSION in kops/haven.sh to the correct tag.
- Update CHANGELOG.md.
- Create a merge request to master.
- Tag the new release with: `git tag v3.0.0 && git push origin v3.0.0`.
- Trigger the manual release jobs to create a new Gitlab release.
- Update the release notes in Project overview > releases with the specific release notes.

## License
Copyright © 2019-2020 VNG Realisatie<br />
Licensed under EUPL v1.2
