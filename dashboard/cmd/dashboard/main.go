// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package main

import (
	"log"

	"github.com/jessevdk/go-flags"
	"go.uber.org/zap"

	"gitlab.com/commonground/haven/haven/dashboard/internal/api"
	"gitlab.com/commonground/haven/haven/dashboard/pkg/git"
	"gitlab.com/commonground/haven/haven/dashboard/pkg/helm"
	"gitlab.com/commonground/haven/haven/dashboard/pkg/oidc"
)

type oidcOptions = oidc.Options

var version = "undefined"

var options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8080" description:"Address for the api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	StaticDir     string `long:"static-dir" env:"STATIC_DIR" default:"" description:"The folder containing the static frontend assets."`
	oidcOptions
}

func main() {
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}

		log.Fatalf("error parsing flags: %v", err)
	}

	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}

	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("cannot initialize zap logger: %v", err)
	}
	defer logger.Sync()

	logger.Info("starting dashboard api", zap.String("listen-address", options.ListenAddress))

	authenticator := oidc.NewAuthenticator(logger, &options.oidcOptions)
	helmClient := helm.NewHelm(logger)
	gitClient := git.NewGit(logger)

	api := api.NewAPI(logger, authenticator, helmClient, gitClient, options.StaticDir)

	err = api.ListenAndServe(options.ListenAddress)
	if err != nil {
		logger.Fatal("failed to listen and serve", zap.Error(err))
	}
}
