# Dashboard

The Haven dashboard allows administrators to manage Helm charts on their cluster from a user-friendly user interface.

![Screenshot of the Haven dashboard](/uploads/852c5c4d02bbbda7e8ee6897f19aac46/image.png)

## Install the dashboard

First generate a password hash for the admin account with:

```bash
htpasswd -bnBC 10 "" {mypassword} | tr -d ':' | sed 's/\$/\\$/g'
```

Replace `{mypassword}` with the password of your choice.

In the command below replace `config.staticPasswords[0].hash` with the hash you generated above. Then install the [Dex](https://github.com/dexidp/dex) OpenID Connect provider with:

```bash
helm repo add commonground https://charts.commonground.nl/
helm repo update
```

```bash
helm upgrade \
  --install \
  --namespace default \
  --set "config.issuer=http://dex.minikube" \
  --set "config.staticPasswords[0].email=admin@example.com" \
  --set "config.staticPasswords[0].hash=\$2y\$10\$9msv7wrW0AswoGoDMpNkHOYQqByTdnqKFyYXvzOFzxwwW0nUSxM5a" \
  --set "config.staticPasswords[0].username=admin" \
  --set "config.staticClients[0].id=dashboard" \
  --set "config.staticClients[0].redirectURIs[0]=http://dashboard.minikube/oidc/callback" \
  --set "config.staticClients[0].name=dashboard" \
  --set "config.staticClients[0].secret=somethingsecret" \
  --set "ingress.enabled=true" \
  --set "ingress.hosts[0]=dex.minikube" \
  dex \
  commonground/dex
```

Generate a random string with:

```bash
openssl rand -hex 32
```

In the command below replace `secretKey` with a random string. Then install the dashboard with:

```bash
helm upgrade \
  --install \
  --namespace default \
  --set "oidcDiscoveryUrl=http://dex.minikube" \
  --set "oidcClientId=dashboard" \
  --set "oidcClientSecret=somethingsecret" \
  --set "oidcRedirectUrl=http://dashboard.minikube/oidc/callback" \
  --set "secretKey=somethingsecret" \
  --set "rbac.clusterAdmin=true" \
  --set "ingress.enabled=true" \
  --set "ingress.hosts[0]=dashboard.minikube" \
  haven-dashboard \
  ./helm
```

The setting `rbac.clusterAdmin=true` gives the dashboard access to the full cluster.

You should now be able to see the dashboard on http://dashboard.minikube/.

## Setup development environment

First start Dex with:

```bash
docker-compose up -d
```

Then start the API with:

```bash
go run cmd/dashboard/main.go \
  --listen-address=localhost:8080 \
  --oidc-discovery-url=http://localhost:5556 \
  --oidc-client-id=dashboard \
  --oidc-client-secret=somethingsecret \
  --oidc-redirect-url=http://localhost:8080/oidc/callback \
  --secret-key=somethingsecret
```

Then use the following commands to start the UI:

```bash
cd ui/
npm install
npm start
```

Go to https://localhost:3000/ to view the dashboard.
