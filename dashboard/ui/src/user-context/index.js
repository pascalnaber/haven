// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useState, useEffect } from 'react'
import { string, func, node, shape } from 'prop-types'
import services from '../services'

const UserContext = React.createContext()

const UserContextProvider = ({
  children,
  user: defaultUser,
}) => {
  const [user, setUser] = useState(defaultUser || null)
  const [isReady, setReady] = useState(false)

  useEffect(
    () => {
      const fetchUser = async () => {
        try {
          const me = await services.oidc.me()
          setUser(me)
        } catch (err) {
          setUser(null)
        }

        setReady(true)
      }

      if (defaultUser) {
        setReady(true)
        return
      }

      fetchUser()
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  )

  return (
    <UserContext.Provider
      value={{
        user: user,
        isReady: isReady,
      }}
    >
      {children}
    </UserContext.Provider>
  )
}

UserContextProvider.propTypes = {
  fetchAuthenticatedUser: func,
  children: node,
  user: shape({
    id: string,
    fullName: string,
    email: string,
    pictureUrl: string,
  }),
}

UserContextProvider.defaultProps = {}

export default UserContext
export { UserContextProvider }
