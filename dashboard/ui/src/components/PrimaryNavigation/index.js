// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { useTranslation } from 'react-i18next'
import {
  StyledHomeLink,
  StyledLink,
  StyledIcon,
  StyledNav,
} from './index.styles'

import { ReactComponent as IconLogo } from './icons/haven.svg'
import { ReactComponent as IconReleases } from './icons/releases.svg'
import { ReactComponent as IconNamespaces } from './icons/namespaces.svg'

const PrimaryNavigation = () => {
  const { t } = useTranslation()
  return (
    <StyledNav>
      <StyledHomeLink
        to="/"
        title={t('Dashboard')}
        aria-label={t('Dashboard')}
      >
        <StyledIcon as={IconLogo} />
        {t('Haven')}
      </StyledHomeLink>

      <StyledLink
        to="/releases"
        title={t('Releases')}
        aria-label={t('Releases')}
      >
        <StyledIcon as={IconReleases} />
        {t('Releases')}
      </StyledLink>

      <StyledLink
        to="/namespaces"
        title={t('Namespaces')}
        aria-label={t('Namespaces')}
      >
        <StyledIcon as={IconNamespaces} />
        {t('Namespaces')}
      </StyledLink>
    </StyledNav>
  )
}

export default PrimaryNavigation
