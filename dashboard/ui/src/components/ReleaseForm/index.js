// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useTranslation } from 'react-i18next'
import { Button, Fieldset, Legend, TextInput } from '@commonground/design-system'

import { Form } from './index.styles'

const DEFAULT_INITIAL_VALUES = {
  'name': 'hallo-wereld',
  'namespace': 'default',
  'chart': {
    'git': 'https://gitlab.com/commonground/haven/hallo-wereld.git',
    'path': 'helm/'
  }
}

const ReleaseForm = ({
  initialValues,
  disableForm,
  onSubmitHandler,
  submitButtonText,
}) => {
  const { t } = useTranslation()

  const validationSchema = Yup.object().shape({
    name: Yup.string().required(),
    namespace: Yup.string().required(),
    chart: Yup.object().shape({
      git: Yup.string().required()
    })
  })

  return (
    <Formik
      initialValues={{ ...DEFAULT_INITIAL_VALUES, ...initialValues }}
      validationSchema={validationSchema}
      onSubmit={onSubmitHandler}
    >
      {({ handleSubmit }) => (
        <Form onSubmit={handleSubmit}>
          <Fieldset>
            <TextInput
              name="name"
              size="l"
              disabled={initialValues ? true : false}
            >
              {t('Name')}
            </TextInput>
            <TextInput
              name="namespace"
              size="l"
            >
              {t('Namespace')}
            </TextInput>
          </Fieldset>

          <Fieldset>
            <Legend>{t('Chart')}</Legend>
            <TextInput
              name="chart.git"
              size="l"
            >
              {t('Git repository')}
            </TextInput>
            <TextInput
              name="chart.path"
              size="l"
            >
              {t('Path')}
            </TextInput>
          </Fieldset>

          <Button type="submit" disabled={disableForm}>{submitButtonText}</Button>
        </Form>
      )}
    </Formik>
  )
}

export default ReleaseForm
