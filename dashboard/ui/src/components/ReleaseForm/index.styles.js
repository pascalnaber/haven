import styled from 'styled-components'

export const Form = styled.form`
  margin-bottom: ${(p) => p.theme.tokens.spacing10};
`
