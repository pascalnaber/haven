export class ReleasesService {
  async list() {
    const result = await fetch('/api/v1/releases')
    if (!result.ok) {
      throw new Error('error occured while getting releases list')
    }

    return result.json()
  }

  async get(namespace, name) {
    const result = await fetch(`/api/v1/releases/${namespace}/${name}`)
    if (!result.ok) {
      throw new Error('error occured while getting release')
    }

    return result.json()
  }

  async create(release) {
    const result = await fetch(`/api/v1/releases`, { method: 'POST', body: JSON.stringify(release) })

    if (result.status === 400) {
      const data = await result.json()
      throw new Error(data.error)
    }

    if (!result.ok) {
      throw new Error('error occured while creating release')
    }

    return result.json()
  }

  async update(namespace, name, release) {
    const result = await fetch(`/api/v1/releases/${namespace}/${name}`, { method: 'PUT', body: JSON.stringify(release) })

    if (result.status === 400) {
      const data = await result.json()
      throw new Error(data.error)
    }

    if (!result.ok) {
      throw new Error('error occured while updating release')
    }

    return result.json()
  }

  async delete(namespace, name) {
    const result = await fetch(`/api/v1/releases/${namespace}/${name}`, { method: 'DELETE'})
    if (!result.ok) {
      throw new Error('error occured while deleting release')
    }

    return result.json()
  }
}
