export class NamespacesService {
  async list() {
    const result = await fetch('/api/v1/namespaces')
    if (!result.ok) {
      throw new Error('error occured while getting namespace list')
    }

    return result.json()
  }
}
