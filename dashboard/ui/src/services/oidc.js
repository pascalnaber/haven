export class OIDCService {
  async me() {
    const result = await fetch('/oidc/me')
    if (!result.ok) {
      throw new Error('an error occured while getting userinfo')
    }

    return result.json()
  }
}
