// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import LoginPage from '../pages/LoginPage/index'
import AuthenticatedRoute from './authenticated-route'
import NotFoundPage from '../pages/NotFoundPage'
import ListReleasesPage from '../pages/ListReleasesPage'
import AddReleasePage from '../pages/AddReleasePage'
import EditReleasePage from '../pages/EditReleasePage'
import NamespacesPage from '../pages/NamespacesPage'

const Routes = () => {
  return (
    <Switch>
      <Redirect exact path="/" to="/releases" />
      <Route path="/login" component={LoginPage} />
      <AuthenticatedRoute path="/releases/add" component={AddReleasePage} />
      <AuthenticatedRoute path="/releases/:namespace/:name/edit" component={EditReleasePage} />
      <AuthenticatedRoute path="/releases" component={ListReleasesPage} />
      <AuthenticatedRoute path="/namespaces" component={NamespacesPage} />
      <Route path="*" component={NotFoundPage} />
    </Switch>
  )
}

export default Routes
