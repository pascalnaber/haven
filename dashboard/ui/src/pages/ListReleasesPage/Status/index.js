// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { useTranslation } from 'react-i18next'

import { ReactComponent as IconStatusUp } from './status-up.svg'
import { ReactComponent as IconStatusDown } from './status-down.svg'
import { ReactComponent as IconStatusDegraded } from './status-degraded.svg'
import { ReactComponent as IconStatusUnknown } from './status-unknown.svg'
import { StyledWrapper } from './index.styles'

const Status = ({ status }) => {
  const { t } = useTranslation()

  return (
    <StyledWrapper>
      {
        {
          deployed: <IconStatusUp title={t('Deployed')} />,
          degraded: <IconStatusDegraded title={t('Degraded')} />,
          progressing: <IconStatusUnknown title={t('Progressing')} />,
          missing: <IconStatusDown title={t('Missing')} />,
        }[status]
      }
    </StyledWrapper>
  )
}

export default Status
