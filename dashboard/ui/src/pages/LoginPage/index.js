// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from '@commonground/design-system'
import { StyledContainer, StyledContent, StyledSidebar, StyledExternalLink } from './index.styles'

const LoginPage = () => {
  const { t } = useTranslation()

  return (
    <StyledContainer>
      <StyledSidebar>
      </StyledSidebar>
      <StyledContent>
        <h1>{t('Welcome')}</h1>
        <Button data-testid="login" as="a" href="/oidc/authenticate">
          {t('Log in with organization account')} <StyledExternalLink />
        </Button>
      </StyledContent>
    </StyledContainer>
  )
}

export default LoginPage
