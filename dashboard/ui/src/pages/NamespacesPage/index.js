// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Alert, Table } from '@commonground/design-system'
import PageTemplate from '../../components/PageTemplate'
import EmptyContentMessage from '../../components/EmptyContentMessage'
import LoadingMessage from '../../components/LoadingMessage'
import usePromise from '../../hooks/use-promise'
import services from '../../services'
import { StyledActionsBar, StyledCount, StyledAmount } from './index.styles'

const NamespaceRow = ({ metadata }) => (
  <Table.Tr name={metadata.name}>
    <Table.Td>{metadata.name}</Table.Td>
  </Table.Tr>
)

const NamespacesPage = () => {
  const { t } = useTranslation()
  const { isReady, error, result } = usePromise(services.namespaces.list)

  return (
    <PageTemplate>
      <PageTemplate.Header title={t('Namespaces')} />

      <StyledActionsBar>
        <StyledCount>
          <StyledAmount>{result ? result.length : 0}</StyledAmount>
          <small>{t('Namespaces')}</small>
        </StyledCount>
      </StyledActionsBar>

      {!isReady ? (
        <LoadingMessage />
      ) : error ? (
        <Alert variant="error">
          {t('Failed to load namespaces.')}
        </Alert>
      ) : result != null && result.length === 0 ? (
        <EmptyContentMessage>
          {t('There are no namespaces yet.')}
        </EmptyContentMessage>
      ) : result ? (
        <Table role="grid">
          <thead>
            <Table.TrHead>
              <Table.Th>{t('Name')}</Table.Th>
            </Table.TrHead>
          </thead>
          <tbody>
            {result.map((namespace, i) => (
              <NamespaceRow key={i} {...namespace} />
            ))}
          </tbody>
        </Table>
      ) : null}
    </PageTemplate>
  )
}

export default NamespacesPage
