// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Alert } from '@commonground/design-system'
import PageTemplate from '../../components/PageTemplate'
import ReleaseForm from '../../components/ReleaseForm'
import services from '../../services'

const AddReleasePage = () => {
  const { t } = useTranslation()
  const [ isProcessing, setIsProcessing ] = useState(false)
  const [ isAdded, setIsAdded ] = useState(false)
  const [ error, setError ] = useState(null)

  const submitRelease = async (release) => {
    setError(false)
    setIsProcessing(true)

    try {
      await services.releases.create(release)
      setIsAdded(true)
    } catch (e) {
      setError(e.message)
    }

    setIsProcessing(false)
  }

  return (
    <PageTemplate>
      <PageTemplate.HeaderWithBackNavigation
        backButtonTo="/releases"
        title={t('Add release')}
      />

      {error ? (
        <Alert
          title={t('Failed adding release')}
          variant="error"
        >
          {error}
        </Alert>
      ) : null}

      {isAdded && !error ? (
        <Alert variant="success">
          {t('The release has been added.')}
        </Alert>
      ) : null}

      {!isAdded ? (
        <ReleaseForm submitButtonText="Add" onSubmitHandler={submitRelease} disableForm={isProcessing} />
      ) : null}
    </PageTemplate>
  )
}

export default AddReleasePage
