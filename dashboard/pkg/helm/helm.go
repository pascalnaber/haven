// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package helm

import (
	"fmt"
	"log"
	"os"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/cli"
	"helm.sh/helm/v3/pkg/downloader"
	"helm.sh/helm/v3/pkg/getter"
	"helm.sh/helm/v3/pkg/release"

	"gitlab.com/commonground/haven/haven/dashboard/pkg/kubernetes"
)

// Helm wraps the Helm API
type Helm struct {
	logger *zap.Logger
}

// NewHelm constructs an instance of Helm
func NewHelm(l *zap.Logger) *Helm {
	h := &Helm{
		logger: l,
	}

	return h
}

// ListReleases returns a list of installed Helm releases
func (h *Helm) ListReleases() ([]*release.Release, error) {
	kubernetesClient, err := kubernetes.GetConfig("")
	if err != nil {
		h.logger.Error("could not initialize Kubernetes config", zap.Error(err))
		return nil, err
	}

	actionConfig := new(action.Configuration)
	if err := actionConfig.Init(kubernetesClient, "", os.Getenv("HELM_DRIVER"), log.Printf); err != nil {
		h.logger.Error("error occured while initializing Helm", zap.Error(err))
		return nil, err
	}

	client := action.NewList(actionConfig)
	results, err := client.Run()
	if err != nil {
		h.logger.Error("error occured while listing releases", zap.Error(err))
		return nil, err
	}

	return results, err
}

// GetRelease returns a Helm release by name
func (h *Helm) GetRelease(namespace string, name string) (*release.Release, error) {
	kubernetesClient, err := kubernetes.GetConfig(namespace)
	if err != nil {
		h.logger.Error("could not initialize Kubernetes config", zap.Error(err))
		return nil, err
	}

	actionConfig := new(action.Configuration)

	if err := actionConfig.Init(kubernetesClient, namespace, os.Getenv("HELM_DRIVER"), log.Printf); err != nil {
		h.logger.Error("error occured while initializing Helm", zap.Error(err))
		return nil, err
	}

	client := action.NewGet(actionConfig)
	result, err := client.Run(name)
	if err != nil {
		h.logger.Error("error occured while getting release", zap.Error(err))
		return nil, err
	}

	return result, err
}

// Install installs a specific Helm chart
func (h *Helm) Install(namespace string, name string, chartPath string, values map[string]interface{}) (*release.Release, error) {
	kubernetesClient, err := kubernetes.GetConfig(namespace)
	if err != nil {
		h.logger.Error("could not initialize Kubernetes config", zap.Error(err))
		return nil, err
	}

	actionConfig := new(action.Configuration)
	if err := actionConfig.Init(kubernetesClient, namespace, os.Getenv("HELM_DRIVER"), log.Printf); err != nil {
		h.logger.Error("error occured while initializing Helm", zap.Error(err))
		return nil, err
	}

	chartRequested, err := loader.Load(chartPath)
	if err != nil {
		h.logger.Error("chart is not loadable", zap.Error(err))
		return nil, err
	}

	validInstallableChart, err := isChartInstallable(chartRequested)
	if !validInstallableChart || err != nil {
		h.logger.Error("chart is not installable", zap.Error(err))
		return nil, err
	}

	if chartRequested.Metadata.Deprecated {
		h.logger.Info("chart is deprecated")
	}

	client := action.NewInstall(actionConfig)
	client.CreateNamespace = true
	client.Namespace = namespace
	client.ReleaseName = name

	fetchedDependencies, err := fetchChartDependencies(chartRequested, chartPath, client.ChartPathOptions.Keyring)
	if err != nil {
		return nil, errors.Wrap(err, "failed reloading chart after repo update")
	}

	if fetchedDependencies {
		// Reload the chart with the updated Chart.lock file.
		chartRequested, err = loader.Load(chartPath)
		if err != nil {
			return nil, errors.Wrap(err, "failed reloading chart after repo update")
		}
	}

	return client.Run(chartRequested, values)
}

// Upgrade upgrades a specific release
func (h *Helm) Upgrade(namespace string, name string, chartPath string, values map[string]interface{}) (*release.Release, error) {
	kubernetesClient, err := kubernetes.GetConfig(namespace)
	if err != nil {
		h.logger.Error("could not initialize Kubernetes config", zap.Error(err))
		return nil, err
	}

	actionConfig := new(action.Configuration)
	if err := actionConfig.Init(kubernetesClient, namespace, os.Getenv("HELM_DRIVER"), log.Printf); err != nil {
		h.logger.Error("error occured while initializing Helm", zap.Error(err))
		return nil, err
	}

	chartRequested, err := loader.Load(chartPath)
	if err != nil {
		h.logger.Error("chart is not loadable", zap.Error(err))
		return nil, err
	}

	validInstallableChart, err := isChartInstallable(chartRequested)
	if !validInstallableChart || err != nil {
		h.logger.Error("chart is not installable", zap.Error(err))
		return nil, err
	}

	if chartRequested.Metadata.Deprecated {
		h.logger.Info("chart is deprecated")
	}

	client := action.NewUpgrade(actionConfig)

	fetchedDependencies, err := fetchChartDependencies(chartRequested, chartPath, client.ChartPathOptions.Keyring)
	if err != nil {
		return nil, errors.Wrap(err, "failed reloading chart after repo update")
	}

	if fetchedDependencies {
		// Reload the chart with the updated Chart.lock file.
		chartRequested, err = loader.Load(chartPath)
		if err != nil {
			return nil, errors.Wrap(err, "failed reloading chart after repo update")
		}
	}

	release, err := client.Run(name, chartRequested, values)
	if err != nil {
		h.logger.Error("error occured while upgrading release", zap.Error(err))
		return nil, err
	}

	return release, nil
}

// Uninstall uninstalls a specific release
func (h *Helm) Uninstall(namespace string, name string) (*release.UninstallReleaseResponse, error) {
	kubernetesClient, err := kubernetes.GetConfig(namespace)
	if err != nil {
		h.logger.Error("could not initialize Kubernetes config", zap.Error(err))
		return nil, err
	}

	actionConfig := new(action.Configuration)
	if err := actionConfig.Init(kubernetesClient, namespace, os.Getenv("HELM_DRIVER"), log.Printf); err != nil {
		h.logger.Error("error occured while initializing Helm", zap.Error(err))
		return nil, err
	}

	client := action.NewUninstall(actionConfig)
	result, err := client.Run(name)
	if err != nil {
		h.logger.Error("error occured while uninstalling release", zap.Error(err))
		return nil, err
	}

	return result, err
}

func (h *Helm) logHelmOutput(format string, args ...interface{}) {
	h.logger.Debug(fmt.Sprintf(format, args...))
}

func isChartInstallable(ch *chart.Chart) (bool, error) {
	switch ch.Metadata.Type {
	case "", "application":
		return true, nil
	}
	return false, errors.Errorf("%s charts are not installable", ch.Metadata.Type)
}

func fetchChartDependencies(chartRequested *chart.Chart, chartPath string, keyring string) (bool, error) {
	if req := chartRequested.Metadata.Dependencies; req != nil {
		// If CheckDependencies returns an error, we have unfulfilled dependencies.
		// As of Helm 2.4.0, this is treated as a stopping condition:
		// https://github.com/helm/helm/issues/2209
		if err := action.CheckDependencies(chartRequested, req); err != nil {
			man := &downloader.Manager{
				Out:        os.Stdout,
				ChartPath:  chartPath,
				Keyring:    keyring,
				SkipUpdate: false,
				Getters:    getter.All(&cli.EnvSettings{}),
			}
			if err := man.Update(); err != nil {
				return false, err
			}
		}

		return true, nil
	}

	return false, nil
}
