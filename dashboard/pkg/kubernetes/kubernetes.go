// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package kubernetes

import (
	"context"
	"os"

	"helm.sh/helm/v3/pkg/cli"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// GetConfig retrieves a Kubernetes configuration for a specific namespace
func GetConfig(namespace string) (*genericclioptions.ConfigFlags, error) {
	var config *rest.Config
	var err error

	if os.Getenv("KUBERNETES_SERVICE_HOST") != "" {
		config, err = rest.InClusterConfig()
		if err != nil {
			return nil, err
		}
	} else {
		settings := cli.New()
		config, err = settings.RESTClientGetter().ToRESTConfig()
		if err != nil {
			return nil, err
		}
	}

	kubeConfig := genericclioptions.NewConfigFlags(false)
	kubeConfig.APIServer = &config.Host
	kubeConfig.BearerToken = &config.BearerToken
	kubeConfig.CAFile = &config.CAFile
	kubeConfig.Namespace = &namespace

	return kubeConfig, nil
}

// ListNamespaces retrieves a list of namespaces in a cluster
func ListNamespaces() ([]v1.Namespace, error) {
	config, err := GetConfig("")
	if err != nil {
		return nil, err
	}

	restConfig, err := config.ToRESTConfig()
	if err != nil {
		return nil, err
	}

	clientset, err := kubernetes.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	services, err := clientset.CoreV1().Namespaces().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return services.Items, nil
}
