// Copyright © VNG Realisatie 2020
// Licensed under EUPL v1.2

package git

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"go.uber.org/zap"
)

// Git wraps go-git
type Git struct {
	logger *zap.Logger
}

// NewGit constructs an instance of Git
func NewGit(l *zap.Logger) *Git {
	g := &Git{
		logger: l,
	}

	return g
}

// CloneRepository fetches a repository and clones the version
func (g *Git) CloneRepository(url string) (string, error) {
	tempDir, err := ioutil.TempDir(os.TempDir(), "haven-gitclone")
	if err != nil {
		g.logger.Error("could not create temporary directory for git clone")
		return "", err
	}

	g.logger.Info(fmt.Sprintf("cloning repository %s to %s", url, tempDir))

	output, err := exec.Command("git", "clone", "--depth", "1", url, tempDir).CombinedOutput()
	if err != nil {
		g.logger.Error(fmt.Sprintf("error while cloning repository: %s %s", err, string(output)))
	}

	return tempDir, err
}
