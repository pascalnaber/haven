// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path"
	"path/filepath"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"go.uber.org/zap"

	"gitlab.com/commonground/haven/haven/dashboard/pkg/git"
	"gitlab.com/commonground/haven/haven/dashboard/pkg/helm"
	"gitlab.com/commonground/haven/haven/dashboard/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/dashboard/pkg/oidc"
)

// API handles incoming access requests and authenticates them
type API struct {
	logger        *zap.Logger
	authenticator *oidc.Authenticator
	helmClient    *helm.Helm
	gitClient     *git.Git
	staticDir     string
}

// Chart holds the configuration of the chart
type Chart struct {
	Git  string
	Path string
}

// Release represents a public-facing Helm release
type Release struct {
	Name      string
	Namespace string
	Chart     Chart
	Values    map[string]interface{}
}

// Error represents an error from the API
type Error struct {
	Error string `json:"error"`
}

// NewError constructs a new instance of Error
func NewError(message string) *Error {
	return &Error{
		Error: message,
	}
}

// NewAPI constructs a new instance of API
func NewAPI(logger *zap.Logger, authenticator *oidc.Authenticator, helmClient *helm.Helm, gitClient *git.Git, staticDir string) *API {
	return &API{
		logger:        logger,
		authenticator: authenticator,
		helmClient:    helmClient,
		gitClient:     gitClient,
		staticDir:     staticDir,
	}
}

// ListenAndServe is a blocking function that listens on provided tcp address to handle requests.
func (a *API) ListenAndServe(address string) error {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Mount("/oidc", a.authenticator.Routes())
	r.Mount("/api/v1", a.authenticator.OnlyAuthenticated(a.Routes()))
	r.Get("/*", a.fileServer)
	return http.ListenAndServe(address, r)
}

func (a *API) fileServer(w http.ResponseWriter, r *http.Request) {
	if a.staticDir == "" {
		http.NotFound(w, r)
		return
	}

	staticDir := http.Dir(a.staticDir)
	fsh := http.FileServer(staticDir)

	_, err := staticDir.Open(path.Clean(r.URL.Path))
	if os.IsNotExist(err) {
		http.ServeFile(w, r, path.Join(a.staticDir, "index.html"))
		return
	}

	fsh.ServeHTTP(w, r)
}

// Routes returns the API routes
func (a *API) Routes() chi.Router {
	r := chi.NewRouter()
	r.Get("/releases", a.listReleases)
	r.Post("/releases", a.createRelease)
	r.Get("/releases/{namespace}/{name}", a.getRelease)
	r.Put("/releases/{namespace}/{name}", a.updateRelease)
	r.Delete("/releases/{namespace}/{name}", a.deleteRelease)
	r.Get("/namespaces", a.listNamespaces)
	return r
}

func (a *API) listReleases(w http.ResponseWriter, r *http.Request) {
	releases, err := a.helmClient.ListReleases()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError("unable to retrieve releases list"))
		return
	}

	render.JSON(w, r, releases)
}

func (a *API) getRelease(w http.ResponseWriter, r *http.Request) {
	namespace := chi.URLParam(r, "namespace")
	name := chi.URLParam(r, "name")

	release, err := a.helmClient.GetRelease(namespace, name)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError("error while getting release"))
		return
	}

	render.JSON(w, r, release)
}

func (a *API) createRelease(w http.ResponseWriter, r *http.Request) {
	var inputRelease *Release

	err := json.NewDecoder(r.Body).Decode(&inputRelease)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError("unable to parse release"))
		return
	}

	repoPath, err := a.gitClient.CloneRepository(inputRelease.Chart.Git)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError(fmt.Sprintf("unable to clone git repository: %s", err)))
		return
	}

	chartPath := filepath.Join(repoPath, inputRelease.Chart.Path)
	installedRelease, err := a.helmClient.Install(inputRelease.Namespace, inputRelease.Name, chartPath, inputRelease.Values)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError(fmt.Sprintf("unable to install release: %s", err)))
		return
	}

	render.JSON(w, r, installedRelease)
}

func (a *API) updateRelease(w http.ResponseWriter, r *http.Request) {
	var inputRelease *Release

	err := json.NewDecoder(r.Body).Decode(&inputRelease)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError(fmt.Sprintf("unable to parse release", err)))
		return
	}

	repoPath, err := a.gitClient.CloneRepository(inputRelease.Chart.Git)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError(fmt.Sprintf("unable to clone git repository: %s", err)))
		return
	}

	chartPath := filepath.Join(repoPath, inputRelease.Chart.Path)
	installedRelease, err := a.helmClient.Upgrade(inputRelease.Namespace, inputRelease.Name, chartPath, inputRelease.Values)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError(fmt.Sprintf("unable to install release: %s", err)))
		return
	}

	render.JSON(w, r, installedRelease)
}

func (a *API) deleteRelease(w http.ResponseWriter, r *http.Request) {
	namespace := chi.URLParam(r, "namespace")
	name := chi.URLParam(r, "name")

	result, err := a.helmClient.Uninstall(namespace, name)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError("unable to delete release"))
		return
	}

	render.JSON(w, r, result)
}

func (a *API) listNamespaces(w http.ResponseWriter, r *http.Request) {
	result, err := kubernetes.ListNamespaces()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, NewError("unable to get namespace list"))
	}

	render.JSON(w, r, result)
}
