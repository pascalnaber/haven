import React from 'react'

import Layout from '../components/Layout'
import Flex from '../components/Flex'
import Box from '../components/Box'
import SEO from '../components/SEO'
import Container from '../components/Container'
import Jumbotron from '../components/Jumbotron'
import Section from '../components/Section'
import ResponsiveImage from '../components/ResponsiveImage'

import helloHaven from '../images/screenshots/hello-haven.png'
import compliancyChecker from '../images/screenshots/compliancy-checker.png'
import metrics from '../images/screenshots/metrics.png'

const IndexPage = () => (
  <Layout>
    <SEO />
    <Jumbotron>
      <h1>Haven</h1>
      <p>Standaard voor cloud-agnostische infrastructuur</p>
    </Jumbotron>
    <Container>
      <Section>
        <Flex scrollable>
          <Box width={1/3}>
            <h2>Standaard en implementatie</h2>
            <p>Haven biedt zowel een standaard op basis van <a href="https://kubernetes.io/">Kubernetes</a> als een referentie implementatie op basis van <a href="https://kops.sigs.k8s.io/">kops</a>.</p>
            <p>De beheerder kan een cluster eenvoudig controleren met de Haven Compliancy Checker.</p>
          </Box>
          <Box width={1/3}>
            <h2>Herbruikbaar</h2>
            <p>Zowel de referentie implementatie als de additionele tools zijn beschikbaar als open source software.</p>
            <p>Hiermee kunnen overheden en leveranciers eenvoudig hun eigen Haven omgevingen opzetten.</p>
          </Box>
          <Box width={1/3}>
            <h2>Geconfigureerd voor veiligheid</h2>
            <p>Een Haven cluster is standaard geconfigureerd voor veiligheid.</p>
            <p>Dit betekent ondersteuning voor Single-Sign-On met OpenID Connect, Role-Based Access Control (RBAC), audit logging, encryption at rest en network policies.</p>
          </Box>
        </Flex>
      </Section>

      <hr />

      <Section>
        <Flex>
          <Box width={2/5}>
            <h2>Beheerbaar vanuit één container</h2>
            <p>
              Alle hulpmiddelen om een cluster te beheren zijn direct beschikbaar:
            </p>
            <ul>
              <li>Openstack CLI</li>
              <li>Kops</li>
              <li>Kubectl</li>
              <li>KubeDB</li>
              <li>Helm</li>
              <li>Haven Compliancy Checker</li>
              <li>Ansible Vault</li>
              <li>Add-ons zoals OpenID Connect, logging and metrics</li>
            </ul>
          </Box>
          <Box width={3/5}>
            <ResponsiveImage src={helloHaven} alt="Schermafbeelding van de Haven CLI" />
          </Box>
        </Flex>
      </Section>

      <Section>
        <Flex>
          <Box width={2/5}>
            <h2>Geautomatiseerde compliancy check</h2>
            <p>
              Met de Haven Compliancy Checker kan een beheerder bestaande clusters geautomatiseerd valideren op Haven compliancy.
            </p>
          </Box>
          <Box width={3/5}>
            <ResponsiveImage src={compliancyChecker} alt="Schermafbeelding van de Haven Compliancy Checker" />
          </Box>
        </Flex>
      </Section>

      <Section>
        <Flex>
          <Box width={2/5}>
            <h2>Inzicht in prestaties</h2>
            <p>
              Haven biedt inzicht in de prestaties van het cluster en applicaties met behulp van <a href="https://prometheus.io/">Prometheus</a>, <a href="https://grafana.com/">Grafana</a>, <a href="https://www.fluentd.org/">Fluentd</a> en <a href="https://www.elastic.co/what-is/elasticsearch">Elasticsearch</a>.
              Ook is het mogelijk om meldingen te ontvangen met <a href="https://elastalert.readthedocs.io/en/latest/">Elastalert</a>.
            </p>
          </Box>
          <Box width={3/5}>
            <ResponsiveImage src={metrics} alt="Schermafbeelding van grafieken die de prestatie van een cluster tonen" />
          </Box>
        </Flex>
      </Section>
    </Container>
  </Layout>
)

export default IndexPage
