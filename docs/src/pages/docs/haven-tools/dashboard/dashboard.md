---
title: "Dashboard"
path: "/docs/dashboard"
---

Het Haven dashboard biedt een gebruikersvriendelijke interface voor beheerders om eenvoudig installaties op een cluster te beheren. In deze sectie laten we zien hoe je het dashboard kunt installeren en gebruiken.

![Schermafbeelding van het Haven dashboard](./schermafbeelding-dashboard.png)

Zorg dat je [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) en [Helm](https://helm.sh/) hebt geïnstalleerd op je lokale computer. Daarnaast heb je cluster admin rechten nodig op een cluster om het dashboard te kunnen installeren.

Je hebt daarnaast een DNS verwijzing nodig voor het dashboard die naar de ingress controller van het cluster verwijst, bijvoorbeeld dashboard.mijn-cluster.nl. Deze verwijzing wordt hieronder *{hostname}* genoemd.

## Single sign-on (SSO)

Het Haven dashboard koppelt met een bestaande inlogbron zodat gebruikersbeheer eenvoudig gecentraliseerd kan worden en additionele eisen zoals twee-factor authenticatie op de inlogbron afgedwongen kunnen worden.

In de volgende stap installeren we [Dex](https://github.com/dexidp/dex) als inlogbron. Hiermee kun je snel aan de slag zonder dat je toegang nodig hebt tot een bestaande inlogbron. het is ook mogelijk om deze stap over te slaan en een bestaande OpenID Connect bron te configureren.

## Inlogbron installeren

Maak eerst een nieuwe namespace aan voor het dashboard:

```bash
$ kubectl create namespace haven-dashboard
```

Genereer vervolgens een hash van het wachtwoord voor het beheerdersaccount:

```bash
$ htpasswd -bnBC 10 "" {mijn-wachtwoord} | tr -d ':'
```

Vervang *{mijn-wachtwoord}* door een wachtwoord naar keuze.

Genereer vervolgens een willekeurig wachtwoord voor de OIDC client:

```bash
$ openssl rand -hex 32
```

Configureer de Common Ground chart repository:

```bash
$ helm repo add commonground https://charts.commonground.nl/
$ helm repo update
```

Installeer vervolgens Dex:

```bash
$ helm install dex commonground/dex \
  --namespace haven-dashboard \
  --set "livenessProbe.httpPath=/dex/healthz" \
  --set "readinessProbe.httpPath=/dex/healthz" \
  --set "config.issuer=https://{hostname}/dex" \
  --set "config.staticPasswords[0].email={email}" \
  --set "config.staticPasswords[0].hash={password-hash}" \
  --set "config.staticPasswords[0].username=admin" \
  --set "config.staticClients[0].id=dashboard" \
  --set "config.staticClients[0].redirectURIs[0]=https://{hostname}/oidc/callback" \
  --set "config.staticClients[0].name=dashboard" \
  --set "config.staticClients[0].secret={oidc-client-secret}" \
  --set "ingress.enabled=true" \
  --set "ingress.path=/dex" \
  --set "ingress.hosts[0]={hostname}"
```

Vervang in het bovenstaande commando

- *{hostname}* door het adres waarop het dashboard bereikbaar moet worden;
- *{email}* door het e-mailadres waarmee je wil inloggen;
- *{password-hash}* door de hash van het wachtwoord die in de vorige stap gegenereerd is en
- *{oidc-client-secret}* door het gegenereerde OIDC client wachtwoord.

## Het Haven dashboard installeren

Het dashboard heeft een willekeurige veilige sleutel nodig. Genereer deze:

```bash
$ openssl rand -hex 32
```

Installeer vervolgens het Haven dashboard:

```bash
$ helm install haven-dashboard commonground/haven-dashboard \
  --namespace haven-dashboard \
  --set "oidcDiscoveryUrl=https://{hostname}/dex" \
  --set "oidcClientId=dashboard" \
  --set "oidcClientSecret={oidc-client-wachtwoord}" \
  --set "oidcRedirectUrl=https://{hostname}/oidc/callback" \
  --set "secretKey={willekeurige-string}" \
  --set "rbac.clusterAdmin=true" \
  --set "ingress.enabled=true" \
  --set "ingress.hosts[0]={hostname}"
```

Vervang hierbij

- *{hostname}* door het adres waarop het dashboard bereikbaar moet worden;
- *{willekeurige-string}* door de willekeurige string die je hierboven hebt gegenereerd en
- *{oidc-client-wachtwoord}* door het OIDC client wachtwoord tijdens de installatie van Dex.

Je kunt vervolgens inloggen op het Haven dashboard via https://{hostname}/. Gebruik het geconfigureerde e-mailadres en wachtwoord om in te loggen.
