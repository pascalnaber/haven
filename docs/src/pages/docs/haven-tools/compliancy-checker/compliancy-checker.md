---
title: "Compliancy checker"
path: "/docs/compliancy-checker"
---

In deze sectie laten we zien hoe je met de compliancy checker een bestaand cluster kan controleren op Haven compliancy.

## Voorbereiding

Zorg dat je [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) hebt geïnstalleerd op je lokale computer. Daarnaast heb je cluster admin rechten nodig om het cluster te valideren op compliancy. Om alle checks uit te voeren heb je ook SSH toegang nodig tot een van de master nodes.

## Installeren

De compliancy checker is onderdeel van de Haven CLI. We bieden de Haven CLI aan voor meerdere besturingssystemen, waaronder macOS, Linux en Windows.

1. Download de [gewenste versie van de Haven CLI](https://gitlab.com/commonground/haven/haven/-/packages)
1. Pak de download uit (`unzip haven-cli-v4.0.zip`)
1. Verplaats de binary in de uitgepakte map naar het gewenste pad (`mv haven-cli /usr/local/bin/haven`)

## Checks uitvoeren

Start de Haven Compliancy checker als volgt:

```bash
$ haven check
```

Binnen enkele ogenblikken toont het systeem de resultaten:

![Schermafbeelding van de Compliancy checker](./schermafbeelding-compliancy-checker.png)

Een cluster is Haven-compliant indien alle checks slagen, inclusief de SSH en CNCF checks. Zie ook `haven check --help`.

## Kubernetes conformance tests

Het is ook mogelijk om de Kubernetes conformance tests uit te voeren. Installeer eerst [Sonobuoy](https://sonobuoy.io/docs/v0.18.4/). Voer daarna de checks uit zoals hierboven vermeld staat. Voeg daarbij de flag `--cncf=true` toe.

## Startpunt

Een Haven-compliant cluster is niet de finishlijn, maar het startpunt. Zorgvuldig beheer is doorlopend van belang voor de operatie en veiligheid, denk hierbij aan:

1. Het up-to-date houden van het cluster.
2. Het up-to-date houden van de deployments op het cluster.
3. Goed beheer voeren op de toegangsrechten tot het cluster.

Vanuit het Haven dashboard is het eenvoudig mogelijk om de eerste toepassingen te deployen op het cluster.

Ontwikkelaars zullen voorts het cluster in gebruik gaan nemen, voor hen hebben we ter illustratie ook de [Ontwikkelen op Haven](/docs/voorbereiding) documentatie pagina's.
