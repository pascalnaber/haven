---
title: "Hallo wereld"
path: "/docs/hallo-wereld"
---

In deze sectie rollen we een eenvoudige applicatie uit op het cluster. Deze applicatie geeft het bericht "Hallo wereld" terug samen met de versie van de applicatie.

![Schermafbeelding van de applicatie](./hallo-wereld.png)

## Broncode en Docker images

De broncode van de applicatie is te vinden op [GitLab](https://www.gitlab.com/commonground/haven/hallo-wereld). Ook staan hier reeds gebouwde [Docker images](https://gitlab.com/commonground/haven/hallo-wereld/container_registry). Voor zowel versie 1.0 als 2.0 is er een tag aanwezig. We gaan de images gebruiken om de applicatie uit te rollen.

## Een deployment maken

We starten met het aanmaken van een deployment. Maak met een editor het volgende bestand aan:

*hallo-wereld-deployment.yaml*
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hallo-wereld
  namespace: default
  labels:
    app: hallo-wereld
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hallo-wereld
  template:
    metadata:
      labels:
        app: hallo-wereld
    spec:
      containers:
        - name: hallo-wereld
          image: registry.gitlab.com/commonground/haven/hallo-wereld:1.0
          ports:
            - containerPort: 80
          resources:
            limits:
              cpu: '1'
              memory: 256Mi
            requests:
              cpu: 50m
              memory: 128Mi
```

Dit is de minimale configuratie die het cluster nodig heeft om een deployment aan te maken.

Er wordt met deze configuratie één replica gestart van de hallo-wereld container in de namespace met de naam default. De container gebruikt de hallo-wereld Docker image en start versie 1.0.

Maak de deployment aan met:

```bash
$ kubectl apply -f hallo-wereld-deployment.yaml
deployment.apps/hallo-wereld created
```

Bekijk vervolgens de status van de deployments in de default namespace met:

```bash
$ kubectl get deployments
NAME           READY   UP-TO-DATE   AVAILABLE   AGE
hallo-wereld   1/1     1            1           39s
```

Na een tijdje wordt de deployment vanzelf beschikbaar.

De deployment maakt automatisch pods aan. Voor iedere replica van de applicatie wordt er een pod aangemaakt. Je kunt de aangemaakte pods als volgt bekijken:

```bash
$ kubectl get pods -o wide
NAME                            READY   STATUS    RESTARTS   AGE    IP                NODE                       NOMINATED NODE   READINESS GATES
hallo-wereld-867b47957d-swhg5   1/1     Running   0          2m3s   100.106.228.234   nodes-3-dv-dev-k8s-local   <none>           <none>
```

Als je in het venster hierboven hierboven wat naar rechts scrollt zie je dat de deployment op nodes-3 terecht is gekomen.

Meer informatie over deployments vind je op [Kubernetes.io](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/).

## Een service maken

Om te zorgen dat onze deployment bereikbaar is binnen het cluster op een vast adres hebben we een service nodig. Maak het volgende bestand aan:

*hallo-wereld-service.yaml*
```yaml
apiVersion: v1
kind: Service
metadata:
  name: hallo-wereld
  namespace: default
spec:
  selector:
    app: hallo-wereld
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

De selector geeft aan hoe de service gekoppeld wordt aan de deployment. In dit geval worden pods met het label `app: hallo-wereld` gekoppeld aan deze service.

Maak vervolgens de service aan met:

```bash
$ kubectl apply -f hallo-wereld-service.yaml
service/hallo-wereld created
```

Bekijk daarna de lijst met services:

```bash
$ kubectl get services -o wide
NAME           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE   SELECTOR
hallo-wereld   ClusterIP   100.66.81.208   <none>        80/TCP    56s   app=hallo-wereld
kubernetes     ClusterIP   100.64.0.1      <none>        443/TCP   51d   <none>
```

Je ziet dat de service is aangemaakt en een Cluster IP heeft gekregen. Binnen het hele cluster kan de applicatie nu op dit IP bereikt worden. Ook is automatisch de DNS verwijzing *hallo-wereld.default* aangemaakt.

Meer informatie over services vind je op [Kubernetes.io](https://kubernetes.io/docs/concepts/services-networking/service/).

## De service bevragen

We gaan nu de service bevragen om te kijken of het allemaal werkt. Hiervoor zetten we eerst met kubectl een port forward op, omdat de service nu alleen nog maar intern bereikbaar is:

```bash
$ kubectl port-forward services/hallo-wereld 8080:80
Forwarding from 127.0.0.1:8080 -> 80
```

Nu wordt poort 8080 op je lokale machine doorgestuurd naar poort 80 van de service in het cluster. Ga nu met de browser naar http://localhost:8080/ om de applicatie te bekijken.

## Versie vernieuwen

Om de applicatie te verhogen naar versie 2.0 pas je in het bestand *hallo-wereld-deployment.yaml* de image aan naar het volgende:

```yaml
    spec:
      containers:
      - name: hallo-wereld
        image: registry.gitlab.com/commonground/haven/hallo-wereld:2.0
```

Vernieuw vervolgens de deployment met:

```yaml
$ kubectl apply -f hallo-wereld-deployment.yaml
deployment.apps/hallo-wereld configured
```

Na een tijdje zal versie 2.0 van de applicatie beschikbaar zijn.

## Verwijderen

Je kunt de deployment en service weer verwijderen met:

```bash
$ kubectl delete -f hallo-wereld-deployment.yaml
deployment.apps "hallo-wereld" deleted
$ kubectl delete -f hallo-wereld-service.yaml
service "hallo-wereld" deleted
```
