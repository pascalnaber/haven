---
title: "Geautomatiseerd uitrollen"
path: "/docs/geautomatiseerd-uitrollen"
---

Wanneer je een applicatie ontwikkelt is het mogelijk om deze applicatie geautomatiseerd te testen en vervolgens uit te rollen op een cluster. Dit wordt ook wel Continous Integration (CI) en Continous Delivery (CD) genoemd.

In deze sectie automatiseren we het uitrollen van een applicatie met behulp van [Flux](https://fluxcd.io/). Flux werkt volgens het GitOps patroon. Hierbij staat alle configuratie van het Kubernetes cluster in een Git repository en volgt het cluster de repository. Een wijziging kan alleen plaatsvinden door de Git repository aan te passen. Deze aanpassing kan handmatig door beheerders gedaan worden, maar ook geautomatiseerd door Flux.

Vraag de beheerder van het cluster om eenmalig de Helm operator Custom Resource Definitions (CRD's) te installeren op het cluster. Dat kan met het volgende commando:

```bash
$ kubectl apply -f https://raw.githubusercontent.com/fluxcd/helm-operator/master/deploy/crds.yaml
```

Fork vervolgens de [voorbeeldrepository op GitLab](https://gitlab.com/commonground/haven/flux-voorbeeld) zodat je deze kunt aanpassen.

In de repository vind je twee mappen:

- chart: hierin zit een Helm chart voor de hallo-wereld applicatie
- releases: hierin zit de configuratie voor het installeren van de Helm chart

Deze standaard configuratie installeert versie 1.0 van de chart hallo-wereld in de test namespace. Pas dit aan naar de namespace die jij gebruikt in het bestand releases/hallo-wereld/hallo-wereld.yaml.

## Flux operator

Installeer vervolgens met behulp van Helm de Flux operator op het cluster. Pas in de configuratie hieronder git.url aan naar het adres van jouw fork en namespace naar de plek waar je Flux en de helm-operator wil installeren.

```bash
$ helm repo add fluxcd https://charts.fluxcd.io

$ helm upgrade --install flux fluxcd/flux \
--namespace test \
--set git.url=git@gitlab.com:commonground/haven/flux-voorbeeld.git \
--set git.pollInterval=5m \
--set registry.includeImage="registry.gitlab.com/commonground*" \
--set additionalArgs={--sync-garbage-collection} \
--set clusterRole.create=false \
--set rbac.create=false

$ helm upgrade -i helm-operator fluxcd/helm-operator \
--namespace test \
--set git.ssh.secretName=flux-git-deploy \
--set helm.versions=v3 \
--set clusterRole.create=false \
--set rbac.create=false \
--skip-crds
```

Maak vervolgens het volgende bestand aan om de rollen voor Flux en de Helm operator toe te wijzen. Vergeet ook hier niet om de namespace meerdere keren aan te passen:

*flux-rolebindings.yaml*

```bash
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  name: flux-admin
  namespace: test
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - name: flux
    namespace: test
    kind: ServiceAccount
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  name: helm-operator-admin
  namespace: test
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - name: helm-operator
    namespace: test
    kind: ServiceAccount
```

Pas deze configuratie op het cluster toe met:

```bash
kubectl apply -f flux-rolebindings.yaml
```

Na een paar minuten is de Flux operator beschikbaar in het cluster.

De operator heeft automatisch een public en private key voor zichzelf gegenereerd. Haal de public key op met:

```bash
$ kubectl -n test logs deployment/flux | grep identity.pub | cut -d '"' -f2
```

Geef nu de Flux operator toegang door de public key te registreren in de repository. In GitLab gaat dit via Settings > Repository > Deploy keys. Vergeet niet de optie "Write access allowed" aan te vinken zodat Flux ook aanpassingen kan maken in de repository. Dit is nodig voor automatische updates.

Flux zal nu automatisch binnen enkele minuten de Git repository volgen en vervolgens de Helm chart hallo-wereld te installeren zoals geconfigureerd staat in het bestand releases/hallo-wereld/hallo-wereld.yaml.

## Handmatig uitrollen

Wanneer je een aanpassing in de repository maakt, zal deze automatisch gesynchroniseerd worden met het cluster. Pas het bestand van de release als volgt aan:

*releases/hallo-wereld/hallo-wereld.yaml:*
```yaml
...
  values:
    image:
      repository: registry.gitlab.com/commonground/haven/hallo-wereld
      tag: "2.0"
```

Commit en push de wijziging naar de repository:

```bash
$ git add .
$ git commit -m "Versie aangepast naar 2.0"
$ git push origin master
```

De wijziging wordt nu binnen ongeveer een minuut gesynchroniseerd met het cluster. Je ziet nu dat de deployment is uitgerold met versie 2.0.

```bash
$ kubectl get deployments -o wide
NAME             READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS           IMAGES                                                    SELECTOR
hallo-wereld     1/1     1            1           14h   hallo-wereld         registry.gitlab.com/commonground/haven/hallo-wereld:2.0   app.kubernetes.io/instance=hallo-wereld,app.kubernetes.io/name=hallo-wereld
```

## Geautomatiseerd uitrollen

Flux biedt ook de mogelijkheid om een container register in de gaten te houden en updates van tags te automatiseren. Voeg hiervoor de volgende annotatie toe aan de metadata:

*releases/hallo-wereld/hallo-wereld.yaml*
```yaml
metadata:
  name: hallo-wereld
  namespace: test
  annotations:
    fluxcd.io/automated: "true"
...
```

Commit en push de wijziging.

Flux controleert nu periodiek of er voor de Docker images van de Helm chart nieuwe tags aanwezig zijn. Als dat het geval is dan commit Flux automatisch de wijziging naar de repository. We hebben voor de hallo-wereld applicatie al een aantal nieuwe versies beschikbaar. Als het goed is zie je binnen enkele minuten de volgende wijziging terug in de repository:

![Schermafbeelding van een commit, een automatische update van Flux](./flux-auto-update.png)

Let er op dat de Flux operator alleen Docker tags volgt voor images die vallen binnen de optie registry.includeImage hierboven.

Je kunt de automatische updates met de volgende annotaties limiteren:

- filter.fluxcd.io/chart-image: semver:~1.0, update tags alleen binnen een bepaalde versie
- filter.fluxcd.io/chart-image: glob:dev-*, update tags alleen binnen een bepaald patroon

Meer informatie over het gebruik van Flux kun je vinden in de [documentatie](https://docs.fluxcd.io/). Hier staat onder andere ook uitgelegd hoe je fluxctl kan gebruiken om meer inzicht te krijgen in de Flux operator.
