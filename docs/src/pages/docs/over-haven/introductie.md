---
title: "Introductie"
path: "/docs"
---

Haven is een standaard voor cloud-agnostische infrastructuur op basis van [Kubernetes](https://www.kubernetes.io). Daarnaast bieden we een referentie implementatie die gebruikt maakt van [kops](https://kops.sigs.k8s.io/).

## De standaard

Haven schrijft een specifieke configuratie van Kubernetes voor om het geschikt te maken voor gebruik binnen overheden:

- Het cluster voldoet aan de [CNCF Conformance test](https://github.com/cncf/k8s-conformance#certified-kubernetes).
- De Kubernetes versie van het cluster is actueel (laatste stabiele release of maximaal 2 minor versies ouder).
- De control plane van het cluster is High Available uitgevoerd.
- Authenticatie gebeurt via een centrale inlogvoorziening (OpenID Connect).
- Autorisatie vindt plaats door middel van Role-Based Access Control.
- De instellingen Basic authentication en Anonymous authentication staan uit.
- De Kubernetes API is niet bereikbaar via HTTP, alleen via HTTPS.
- Er worden centraal metrics verzameld waarmee de gezondheid van een cluster gemonitord kan worden en capaciteitsplanning uitgevoerd kan worden.
- Er wordt op een centrale plek (container)logs geaggegreerd.
- Het cluster is voorzien van een voorziening om geautomatiseerd databases uit te rollen en te beheren.
- Het is mogelijk om dynamisch storage te provisionen, zowel van het type ReadWriteOnce als ReadWriteMany.
- Worker nodes zijn uitgevoerd met SELinux, Grsecurity, AppArmor of LKRG.
- De netwerklaag biedt mogelijkheden tot het maken van network policies om toegang op netwerkniveau te segmenteren.
- Het is mogelijk om dynamisch LoadBalancers te provisionen gebruikmakend van service type LoadBalancer.

Er zijn verschillende implementaties van leveranciers en cloudproviders waarvan we hebben vastgesteld dat ze Haven-compliant zijn. Middels de [Haven Compliancy Checker](./docs/compliancy-checker) kan een Kubernetes cluster na worden gekeken op Haven-compliancy. Bekijk voor meer informatie de [aan de slag](./docs/aan-de-slag) pagina.

## De referentie implementatie

De referentie implementatie maakt het eenvoudiger om met Haven aan de slag te gaan. De implementatie is te downloaden op [GitLab](https://gitlab.com/commonground/haven/haven). Deze implementatie wordt onder andere gebruikt om verschillende teams bij VNG Realisatie te voorzien van een productie-waardig cluster.

De referentie implementatie kan alleen gebruikt worden in combinatie met OpenStack.
