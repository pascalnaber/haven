---
title: "Amazon Elastic Kubernetes Service (EKS)"
path: "/docs/aan-de-slag/aws"
---

Kies bij services voor "Elastic Kubernetes Service". Klik vervolgens op "Create cluster". Gebruik de volgende instellingen:

- Kubernetes version: kies de laatste stabiele versie
- Cluster service role: maak een rol aan voor het cluster
- Secrets encryption: advies om "envelope encryption" te gebruiken om Kubernetes secrets extra te beschermen
- Networking: maak een nieuwe VPC aan voor het cluster
- Security groups: maak een nieuwe security group aan voor het cluster
- Cluster endpoint access: kies voor public and private of private, beperk toegang wanneer het cluster publiek bereikbaar is
- Logging: naar keuze

Maak na het aanmaken van het cluster de node group aan via Compute > Add Node Group. Kies de nodes naar keuze. Wij adviseren nodes met minimaal 2 vCPU's en 4GB RAM. Hou er rekening mee dat een node voldoende ruimte heeft voor het opslaan van de Docker images. Dit is afhankelijk van de te installeren applicaties. Overweeg ook om auto scaling te gebruiken.

Gebruik na de installatie [deze handleiding](https://docs.aws.amazon.com/eks/latest/userguide/calico.html) om Calico te installeren.

Deze voorbeeldconfiguratie is [ook beschikbaar als Terraform code](https://gitlab.com/commonground/haven/haven/-/tree/master/docs/src/pages/docs/over-haven/aan-de-slag/aws/terraform).

![Schermafbeelding van een Kubernetes-cluster op Amazon Elastic Kubernetes Service (EKS)](./aws.png)
