---
title: "Waarom Haven?"
path: "/docs/waarom-haven"
---

Met Haven kun je applicaties eenvoudiger beheren. De generieke laag van Haven heft verschillen in onderliggende infrastructuur op, zodat een systeembeheerder verschillende applicaties op dezelfde manier kan beheren. Voor softwareontwikkelaars maakt Haven het makkelijker om applicaties voor verschillende organisaties te ontwikkelen. Organisaties worden wendbaarder, omdat ze eenvoudiger kunnen wisselen van onderliggende (cloud-)infrastructuur.

## Eenvoudig beheer

Een systeembeheerder ontvangt één Docker-image in plaats van allerlei verschillende installatieprocedures per applicatie. Hierdoor wordt het een stuk eenvoudiger om veel applicaties tegelijkertijd te beheren. De ontwikkelaar van de applicatie schrijft hiervoor eenmalig een Dockerfile.

## Snel

Installeer binnen enkele minuten applicaties uit bijvoorbeeld de [Common Ground AppStore](https://appstore.commonground.nl/). Het is gemakkelijk om verschillende omgevingen aan te maken, voor bijvoorbeeld testen en productie.

## Veilig

Haven maakt het voor systeembeheerders eenvoudiger om applicaties up-to-date te houden. Daarnaast kunnen alle applicaties geautomatiseerd gescand worden op veiligheid. Haven sluit aan bij de best-practices op het gebied van informatiebeveiliging, zowel vanuit de overheid als de industrie.

## Schaalbaar

Haven biedt de mogelijkheid om applicaties eenvoudig te schalen. Wanneer het aantal gebruikers verandert, kan een applicatie automatisch of handmatig worden opgeschaald of afgeschaald.

## Open standaarden

Haven gebruikt wereldwijde open standaarden zoals het [Open Container Initiative](https://opencontainers.org/) (Docker) en [Kubernetes](https://kubernetes.io/). Door Haven te gebruiken profiteren overheden mee van de wereldwijde technologische ontwikkelingen op het gebied van open source software.
