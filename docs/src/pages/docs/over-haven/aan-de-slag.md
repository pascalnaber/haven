---
title: "Aan de slag"
path: "/docs/aan-de-slag"
---

Er zijn twee manieren om met Haven aan de slag te gaan:

1. Zelf de referentie implementatie installeren op een OpenStack omgeving.
2. De Kubernetes implementatie van een leverancier of cloudprovider gebruiken en deze valideren met de Haven Compliancy Checker.

## De referentie implementatie

De referentie implementatie werkt alleen op nieuwere versies van OpenStack. De OpenStack omgeving dient te beschikken over Swift en LBaasV2.

Voor het opzetten en onderhouden van een cluster op basis van de referentie implementatie is diepgaande technische kennis vereist. Volg de [installatieinstructies](https://gitlab.com/commonground/haven/haven/-/tree/master/kops#creating-a-cluster) om een omgeving in te richten.

## Implementatie van leverancier of cloudprovider

Het is ook mogelijk om een implementatie van een leverancier of cloudprovider te gebruiken en deze vervolgens te valideren met de Haven Compliancy Checker. Een aantal mogelijke cloudoplossingen:

- [Amazon Elastic Kubernetes Service](https://aws.amazon.com/eks/) (EKS) - [Aan de slag](/docs/aan-de-slag/aws)
- [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine) (GKE)
- [IBM Cloud Kubernetes Service](https://www.ibm.com/cloud/container-service/)
- [Microsoft Azure Kubernetes Service](https://azure.microsoft.com/nl-nl/services/kubernetes-service/) (AKS) - [Aan de slag](/docs/aan-de-slag/azure)

En een aantal mogelijke on-premise oplossingen:

- [Red Hat OpenShift](https://www.openshift.com/)
- [VMware Tanzu Kubernetes Grid](https://tanzu.vmware.com/kubernetes-grid)

Wanneer een van de bovenstaande oplossingen gebruikt wordt is het belangrijk om het cluster te valideren met de Haven Compliancy Checker. De specifieke inrichting bepaalt namelijk of het cluster compliant is.

De bovenstaande lijst is niet volledig. Dien een [merge request](https://gitlab.com/commonground/haven/haven/-/merge_requests) in om een nieuwe oplossing voor te stellen voor de lijst.
