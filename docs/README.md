# Documentation
This folder contains the online documentation of Haven.

Use the following commands to start the watch server:

```bash
npm install
npm start
```

Go to http://localhost:8000/ to view the documentation.
