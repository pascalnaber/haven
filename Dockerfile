# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

#####################################################################################################################################

##### [ BUILD: CLI ] #####
FROM        golang:1.15-alpine AS cli
RUN         apk add --no-cache git gcc musl-dev
RUN         go get github.com/markbates/pkger/cmd/pkger

COPY        ./cli /app

ARG         HAVEN_VERSION=undefined

RUN         KUBE_LATEST=$(wget -qO- https://storage.googleapis.com/kubernetes-release/release/stable.txt) && \
            cd /app && \
            pkger && pkger -o cmd/cli && \
            go build \
            -ldflags="-w -s -X 'main.version=$HAVEN_VERSION' -X 'gitlab.com/commonground/haven/haven/cli/pkg/compliancy.kubeLatest=$KUBE_LATEST'" \
            -o haven cmd/cli/*.go && chmod 750 haven

#####################################################################################################################################

##### [ HAVEN ] #####
FROM        alpine

ARG         KOPS_VERSION=1.18.2
ARG         KUBECTL_VERSION=1.18.9
ARG         HELM_VERSION=3.3.4
ARG         KUBEDB_VERSION=0.12.0
ARG         K9S_VERSION=0.22.1
ARG         POSTGRES_OPERATOR_VERSION=1.5.0
ARG         SONOBUOY_VERSION=0.19.0
ARG         DEX_K8S_AUTHENTICATOR_VERSION=1.2.0

# Exports used by provisioners.
ENV         KUBECTL_VERSION=${KUBECTL_VERSION}
ENV         KUBEDB_VERSION=${KUBEDB_VERSION}
ENV         METRICS_CHART_VERSION=2.11.1
ENV         DEX_CHART_VERSION=2.13.0
ENV         CERT_MANAGER_VERSION=0.16.1
ENV         CERT_MANAGER_CHART_VERSION=0.16.1

# [ Base ]
RUN         mkdir -p /opt/haven
ENV         HOME=/opt/haven
WORKDIR     /opt/haven

RUN         apk add --no-cache bash bash-completion vim curl openssh-client build-base git python3 python3-dev py3-pip jq openssl-dev libffi-dev musl-dev apache2-utils
RUN         pip3 install --upgrade pip
RUN         mkdir -p /opt/haven/addons /opt/haven/etc /opt/haven/bin/helpers

# [ CLI ]
# Openstack.
RUN         pip3 install python-openstackclient==5.2.1 python-neutronclient==7.2.0 python-octaviaclient==2.1.0 python-swiftclient==3.1.0
# Kubernetes..
RUN         (curl -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && chmod 755 /usr/local/bin/kubectl)
# Helm.
RUN         (curl -L https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar -xzO linux-amd64/helm > /usr/local/bin/helm && chmod 755 /usr/local/bin/helm)
RUN         helm repo add stable https://charts.helm.sh/stable
RUN         helm repo add appscode https://charts.appscode.com/stable/
RUN         helm repo update
# KubeDB.
RUN         (curl -L -o /usr/local/bin/kubedb https://github.com/kubedb/cli/releases/download/${KUBEDB_VERSION}/kubedb-linux-amd64 && chmod 755 /usr/local/bin/kubedb)
# K9s.
RUN         (curl -L https://github.com/derailed/k9s/releases/download/v${K9S_VERSION}/k9s_Linux_x86_64.tar.gz | tar -xzO k9s > /usr/local/bin/k9s && chmod 755 /usr/local/bin/k9s)

# [ Haven: Kops ]
RUN         (curl -L -o /usr/local/bin/kops https://github.com/kubernetes/kops/releases/download/v${KOPS_VERSION}/kops-linux-amd64 && chmod 755 /usr/local/bin/kops)
COPY        kops/etc/cinder.yaml.dist /opt/haven/etc/
RUN         (cd /opt/haven/etc && git clone https://github.com/kubernetes/cloud-provider-openstack.git && mv cloud-provider-openstack/manifests/cinder-csi-plugin/csi-secret-cinderplugin.yaml cloud-provider-openstack/manifests/cinder-csi-plugin/csi-secret-cinderplugin.yaml._ && rm -f cloud-provider-openstack/manifests/cinder-csi-plugin/csi-cinder-driver.yaml)
COPY        kops/bin/ /opt/haven/bin/

# [ Haven: Bash ]
RUN         ln -s /opt/haven/bin/helpers/_bashrc /opt/haven/.bashrc
RUN         /usr/local/bin/kops completion bash >> /opt/haven/.bashrc
RUN         /usr/local/bin/kubectl completion bash >> /opt/haven/.bashrc
RUN         /usr/local/bin/helm completion bash >> /opt/haven/.bashrc
RUN         /usr/bin/openstack complete >> /opt/haven/.bashrc

# [ Haven: Secrets ]
RUN         pip3 install ansible==2.9.10

# [ Haven: Traefik ]
RUN         mkdir -p addons/traefik
COPY        addons/traefik /opt/haven/addons/traefik

# [ Haven: Monitoring ]
COPY        addons/monitoring /opt/haven/addons/monitoring

# [ Haven: OIDC ]
RUN         mkdir -p addons/oidc
RUN         git clone --depth 1 --single-branch --branch v${DEX_K8S_AUTHENTICATOR_VERSION} https://github.com/mintel/dex-k8s-authenticator.git addons/oidc/dex-k8s-authenticator
COPY        addons/oidc /opt/haven/addons/oidc

# [ Haven: Postgres ]
RUN         mkdir -p addons/postgres
RUN         git clone https://github.com/zalando/postgres-operator.git addons/postgres/postgres-operator
RUN         (cd addons/postgres/postgres-operator; git reset --hard v${POSTGRES_OPERATOR_VERSION})
COPY        addons/postgres /opt/haven/addons/postgres

# [ Haven: Kubernetes dashboard ]
RUN         mkdir -p addons/kubernetes-dashboard
COPY        addons/kubernetes-dashboard /opt/haven/addons/kubernetes-dashboard

# [ Haven: Haven dashboard ]
RUN         mkdir -p addons/haven-dashboard
COPY        addons/haven-dashboard /opt/haven/addons/haven-dashboard

# [ Haven: Open policy agent ]
RUN         mkdir -p addons/open-policy-agent
COPY        addons/open-policy-agent /opt/haven/addons/open-policy-agent

# [ Haven: CLI ]
COPY        --from=cli /app/haven /opt/haven/bin/haven
RUN         (curl -L https://github.com/vmware-tanzu/sonobuoy/releases/download/v${SONOBUOY_VERSION}/sonobuoy_${SONOBUOY_VERSION}_linux_amd64.tar.gz | tar -xzO sonobuoy > /usr/local/bin/sonobuoy && chmod 755 /usr/local/bin/sonobuoy)

# [ Base ]
COPY        docker-entrypoint.sh /docker-entrypoint.sh
RUN         chmod 755 /docker-entrypoint.sh

# Export used by _bashrc.
ARG         HAVEN_VERSION=undefined
ENV         HAVEN_VERSION=${HAVEN_VERSION}

ENTRYPOINT  ["/docker-entrypoint.sh"]
