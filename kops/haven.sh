#!/usr/bin/env bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

# Keep in sync with release tag, see README.md "Release management".
VERSION=latest

if [[ $0 =~ haven.sh$ ]]; then
    echo -e "\n\nPlease consider symlinking or copying haven.sh to /usr/local/bin/haven\n\n"
fi

if [[ "$#" -ne 1 ]]; then
    echo -e "\nUsage: $0 <CLUSTER_NAME>\n\nExample: $0 demo\n\n"

    exit 1
fi

command -v docker >/dev/null || (echo "Required binary 'docker' not found in path. Halting." && exit 1)
command -v gpg >/dev/null || (echo "Required binary 'gpg' not found in path. Halting." && exit 1)

CLUSTER=$1
HAVENPATH=$HOME/.haven
STATEPATH=$HAVENPATH/state/$CLUSTER

if [[ ! -d $STATEPATH ]]; then
    echo -ne "[Haven] Cluster $CLUSTER does not yet exist in Haven. Creating $STATEPATH.\n\n"
    read -rep $'Continue [y/n]: ' CONTINUE
    echo

    if [[ "y" != "$CONTINUE" ]]; then
        echo "ABORTED"

        exit 1
    fi

    mkdir -p $STATEPATH
    cd $STATEPATH

    git init

    echo -ne "-> Please copy cluster.env + openrc.sh + ssh_id_rsa[.pub] to this directory and run Haven again.\n\n"

    exit 1
fi

if [ ! -e $STATEPATH/cluster.env ]; then
    echo "[Haven] Please create $STATEPATH/cluster.env and run Haven again, see haven:kops/etc/cluster.env.dist."

    exit 1
fi

if [ ! -f $STATEPATH/openrc.sh ]; then
    echo "[Haven] Please create $STATEPATH/openrc.sh and run Haven again."

    exit 1
fi

if [[ ! -f $STATEPATH/ssh_id_rsa || ! -f $STATEPATH/ssh_id_rsa.pub ]]; then
    echo "[Haven] Please create your $STATEPATH/ssh_id_rsa[.pub] SSH keypair first and run Haven again."

    exit 1
fi

# Secrets: owners.
if [[ ! -f $STATEPATH/master.key.admins ]]; then
    echo -en "[Haven] Please enter your admin GPG email addresses separated by spaces used for encryption of secrets.\n\tMake sure all related pub keys are in your own keychain.\n\tExample -> admin@haven.vng.cloud hostmaster@vng.nl\n\n\t-> "
    read GPG_ADMINS

    echo $GPG_ADMINS > $STATEPATH/master.key.admins
fi

# Secrets: on Entry of Haven.
if [[ -f $STATEPATH/master.key.gpg ]]; then
    # Decrypt.
    MASTERKEY=$(gpg -d $STATEPATH/master.key.gpg)
else
    # Create.
    MASTERKEY=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c32)

    echo $MASTERKEY | gpg -e -r $(cat $STATEPATH/master.key.admins | sed "s/ / -r /g") -o $STATEPATH/master.key.gpg
fi

# Secrets: on Exit of Haven.
encrypt_on_exit() {
    # By deleting the master key you can set a new one and exit Haven to persist.
    if [[ ! -f $STATEPATH/master.key.gpg ]]; then
        echo $MASTERKEY | gpg -e -r $(cat $STATEPATH/master.key.admins | sed "s/ / -r /g") -o $STATEPATH/master.key.gpg
    fi
}
trap encrypt_on_exit exit

# Run Haven.
docker run --rm -ti -e CLUSTER=$CLUSTER -e MASTERKEY=$MASTERKEY -v $STATEPATH:/opt/haven/state registry.gitlab.com/commonground/haven/haven:$VERSION sh
