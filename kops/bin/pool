#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

function poolhelp {
    echo -e "\nUsage: pool <command> <name>\n\nSupported commands: create\n\n\nExample: pool create monitoring\n"

    exit 1
}

[[ $# -eq 2 ]] || poolhelp
[[ "create" == $1 ]] || poolhelp

COMMAND=$1
POOL=$2

[[ -z $EDITOR ]] && EDITOR=nano

# Creation flow.
if [ "create" == $COMMAND ]; then
    echo -ne "[Haven] This will create a new node pool named '$POOL'.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

    POOLPATH=$STATEPATH/pool-$POOL.yaml

    kops get ig nodes -o yaml > $POOLPATH

    sed -i "/creationTimestamp/d" $POOLPATH
    sed -i "s/  name\: nodes/  name\: $POOL/" $POOLPATH
    sed -i "s/    kops.k8s.io\/instancegroup\: nodes/    kops.k8s.io\/instancegroup\: $POOL/" $POOLPATH

    echo "[Haven] Press enter to edit your new node pool specification."
    read

    $EDITOR $POOLPATH

    kops create -f $POOLPATH

    rm -f $POOLPATH

    echo -e "\n[Haven] Dry running kops update cluster."
    kops update cluster

    echo -e "\n\n[Haven] Press enter to actually create your new node pool."
    read

    kops update cluster --yes
fi

echo -e "\n\n[Haven] Pool maintenance finished."
