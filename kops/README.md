# Kops on Openstack.

Haven requires OpenStack with Swift and LBaaSv2 (preferably Octavia v2.12+). OpenStack allows for a lot of customization, everything works out best with providers that mostly stick to standard OpenStack.

## Creating a cluster

Run `haven <CLUSTER_NAME>` to create a new Haven environment for your cluster. Haven will guide you through the nescessary steps to get started with a new cluster.

From Haven simply run `create` to deploy your cluster.

## Updating a cluster

First ensure you have a backup of everything you can backup. Best practice would be to try all your changes first on an identical Haven cluster, before you apply changes to a production cluster.

You can easily make changes to your cluster, for example changing the worker node flavor, adding bastion security group ip ranges or upgrading Kubernetes.
Simply try `kops edit cluster` or `kops edit ig nodes`.

When you are done editing run `kops update cluster` and confirm changes with `kops update cluster --yes`.
Sometimes you get a message hinting your cluster needs a rolling update, which will replace nodes one by one with newly configured nodes. Run `kops rolling-update cluster` and confirm changes with `kops rolling-update cluster --yes`.

## Upgrading a cluster

First ensure you have a backup of everything you can backup. Best practice would be to try all your changes first on an identical Haven cluster, before you apply changes to a production cluster.

Running a newer version of Haven will automatically detect an available cluster upgrade. You can also manually run upgrades with: `upgrade`.

## Destroying a cluster

From Haven run:

`destroy`

## Cluster management

From Haven these tools are available to you:

`haven check` Runs the Haven Compliancy Checker\
`status` Shows an overview of cluster and pod health\
`create` Creates a new cluster\
`upgrade` Upgrades an existing cluster\
`destroy` Destroys an existing cluster\
`pool` Creates a new node pool\
`ssh bastion` SSH into the bastion host\
`ssh <node ip>` SSH into a node through the bastion host\
`openstack server list` Lists all servers including cluster nodes\
`openstack loadbalancer list` Lists all Octavia loadbalancers\
`neutron lbaas-loadbalancer-list` Lists all LBaaSv2 loadbalancers (legacy)\
`kops` Manages cluster\
`kubectl` Manages kubernetes\
`helm` Manages deployments\
`kubedb` Manages KubeDB databases

Haven ships with bash completion enabled for kops, kubectl, helm and openstack.

### Application deployments

[Helm](https://helm.sh/) is a Kubernetes package manager that can be used to manage deployments on Haven.

If your application requires a HTTP load balancer and SSL certificates, make sure you installed the [Traefik addon](../addons/traefik/README.md).

### Shared storage

Cinder block storage PersistentVolumes are enabled by default for Haven clusters. This is a ReadWriteOnce solution, appropriate for use with for example traefik and kubedb.

Example: [etc/example-pvc.yaml](etc/example-pvc.yaml).

### Databases

If you want or need to deploy your databases inside Kubernetes, consider using KubeDB or the [Postgres Operator addon](../addons/postgres/README.md). Haven automatically deploys KubeDB for you during provisioning.

KubeDB allows you to easily and properly deploy databases like Postgres and MySQL, but also key apps like ElasticSearch. You can also use it for scheduled backups and monitoring.

Example: [etc/example-pg.yaml](etc/example-pg.yaml).

### User management: OpenID Connect (OIDC)

OIDC is the preferred way to manage users on a Kubernetes cluster. You can use your own User store like Active Directory, Gitlab or Github.

Haven allows you to provision OIDC including Identity Provider and Client.

For more information see [../addons/oidc/README.md](../addons/oidc/README.md).

### Node pools

With kops it's very easy to manage the cluster's instance groups (e.g. `kops edit ig nodes`) and create additional node pools.

A typical usecase could be creating a dedicated monitoring node pool running only critical deployments like ElasticSearch and Alerting. Effectively this means your regular application deployments can never take down the monitoring nodes which is critical when troubleshooting issues. Having a dedicated pool you can use a combination of affinity and tolerations to make this work.

To make it easier to create a new node pool having all Haven specifics in the spec you can use Haven's helper like this: `pool create <name>`. The script will duplicate your default nodes instance group and allow you to edit the specifics before actually creating it.

Please refer to the official kops documentation for more in depth information about Instance Groups: [https://github.com/kubernetes/kops/blob/master/docs/instance_groups.md](https://github.com/kubernetes/kops/blob/master/docs/instance_groups.md).

### Secrets

#### Usage

Upon cluster creation Haven asks you for a list of email addresses the master key will be gpg encrypted with. A master key will be generated for you. The master key will transparantly be used with ansible-vault to encrypt / decrypt secret files like your SSH private key and password variables inside yaml templates. Upon exiting Haven the master key is stored gpg encrypted in your state repository.

#### Re-encrypting the master key with a changed list of admins

From inside haven:

1. Edit $STATEPATH/master.key.admins to fit your needs.
2. `rm $STATEPATH/master.key.gpg`
3. `exit`

Haven will recreate master.key.gpg encrypted with the new secrets.admins public keys.

#### Updating an encrypted file

If you want to update openrc.sh for example, which is encrypted in your state repository, simple replace it with the new unencrypted version. When you run Haven it will detect the unencrypted file, use it and encrypt it for you.

#### Updating the master key

For example when someone in your team leaves you should roll all the keys. Problem is: rolling the master key is not good enough. Typically Kubernetes clusters are full of secrets all over the place, including some which cannot be rolled (like the master private certificate).

Therefore there is currently no way to roll the master key. To properly renew keys when you must, there is only one possibility: create a new cluster and migrate your workload.

## Q/A

Q: Where can I find more info?\
A: Everything including sourcecode can be found at https://gitlab.com/commonground/haven/haven. Kanban board direct link: https://gitlab.com/commonground/haven/haven/boards/963655.

Q: What should I expect from a configured cluster?\
A: - Debian powered Kubernetes Master and Worker nodes.
   - All instances are attached to an internal network, only SSH accessible through a bastion host with whitelisted IP ranges.
   - Master API only accessible through a loadbalancer with whitelisted IP ranges.
   - Kubernetes 1.1x.
   - Etcd 3.x.
   - External Cloud Controller Manager including Cinder CSI.
   - Calico networking.
   - Kubernetes RBAC enabled.
   - Kubernetes anonymous API auth enabled (required by RBAC, see https://kubernetes.io/docs/reference/access-authn-authz/authentication/#anonymous-requests).
   - Kubernetes insecure API port disabled.
   - Kubernetes API basic auth disabled, token + OpenID enabled.
   - Helm enabled deployments.
   - Metrics server enabled (allows for kubectl top pods).
   - Traefik/Loadbalancer deployments (SSL enabled, http to https redirects, automated certificate handling using LetsEncrypt).
   - Encrypted secrets at rest.
   - KubeDB enabled, allowing you to easily deploy for example HA Postgres.
   - Patches and workarounds for known issues in any Haven moving parts like 'gophercloud redundant endpoint versioning'.

Q: Can I run multiple clusters in a single OpenStack account/project?\
A: Yes, no specifics required.

Q: Where are all the moving parts stored on my host (e.g. laptop)?\
A: - Haven itself is stored where you've cloned the repository.
   - Cluster state is stored inside ~/.haven/state/<CLUSTER_NAME>

Q: What is the flow, what happens exactly when I open and close Haven?\
A: - When a Haven session is running the environments' state directory will be used as a volume inside the Haven docker container.

## License
Copyright © VNG Realisatie 2019-2020
Licensed under the EUPL
