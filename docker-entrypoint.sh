#!/bin/bash

# Copyright © VNG Realisatie 2019-2020
# Licensed under the EUPL

set -e

export HAVENPATH=/opt/haven
export ETCPATH=$HAVENPATH/etc
export STATEPATH=$HAVENPATH/state

# Secrets.
export KEYFILE=/tmp/master.key
echo $MASTERKEY > $KEYFILE
chmod 600 $KEYFILE

# - Secret: SSH private key. Originally user provisioned decrypted.
if ! head -n 1 $STATEPATH/ssh_id_rsa | grep -q ANSIBLE_VAULT; then
    echo "[Haven] Found unencrypted ssh_id_rsa."

    ansible-vault encrypt $STATEPATH/ssh_id_rsa --vault-password-file $KEYFILE
fi

ansible-vault view $STATEPATH/ssh_id_rsa --vault-password-file $KEYFILE > /tmp/ssh_id_rsa
chmod 600 /tmp/ssh_id_rsa

# - Secret: OpenStack RC. Originally user provisioned decrypted.
if ! head -n 1 $STATEPATH/openrc.sh | grep -q ANSIBLE_VAULT; then
    echo "[Haven] Found unencrypted openrc.sh."

    ansible-vault encrypt $STATEPATH/openrc.sh --vault-password-file $KEYFILE
fi

ansible-vault view $STATEPATH/openrc.sh --vault-password-file $KEYFILE > /tmp/openrc.sh
chmod 600 /tmp/openrc.sh

# - Secret: Kube config. Immediately encrypted after creation by Haven.
if [[ -f $STATEPATH/kubectl.conf ]]; then
    ansible-vault view $STATEPATH/kubectl.conf --vault-password-file $KEYFILE > /tmp/kubectl.conf
    chmod 600 /tmp/kubectl.conf
fi

# Authorize OpenStack.
export OS_DOMAIN_NAME=default

source /tmp/openrc.sh
rm -f /tmp/openrc.sh

# Init kubectl.
export KUBECONFIG=/tmp/kubectl.conf

# Init kops.
export KOPS_STATE_STORE=swift://kops-${CLUSTER}
export KOPS_CLUSTER_NAME=${CLUSTER}.k8s.local
export KOPS_FEATURE_FLAGS=EnableExternalCloudController

# Init state.
if [[ ! -f $STATEPATH/VERSION ]]; then
    echo $HAVEN_VERSION > $STATEPATH/VERSION
fi

# Enable SSH.
if [ ! -f $STATEPATH/ssh_config ]; then
    cat > $STATEPATH/ssh_config <<EOF
ForwardAgent no
ServerAliveInterval 120

# Update 'Hostname' to your bastion ip.
Host bastion
  User debian
  Hostname %BASTION_IP%

# Update range to match your cluster.tf 'subnet_cidr'.
Host 10.0.*.*
  User debian
  ProxyJump bastion
EOF

    echo "[Haven] Provisioned /root/.ssh/config."
fi

if [ ! -f $STATEPATH/ssh_known_hosts ]; then
    touch $STATEPATH/ssh_known_hosts
fi

mkdir /root/.ssh
chown -R root:root /root/.ssh

ln -s $STATEPATH/ssh_config /root/.ssh/config
ln -s $STATEPATH/ssh_known_hosts /root/.ssh/known_hosts
ln -s $STATEPATH/ssh_id_rsa.pub /root/.ssh/id_rsa.pub
ln -s /tmp/ssh_id_rsa /root/.ssh/id_rsa

eval $(ssh-agent -s) >/dev/null
ssh-add /root/.ssh/id_rsa

# Hello Haven.
cd /opt/haven

export PATH=/opt/haven/bin:$PATH
export PS1="[Haven: $CLUSTER] \w # "

exec /bin/bash
